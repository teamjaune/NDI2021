package xyz.teamjaune.ndi.controller.rescue;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import xyz.teamjaune.ndi.controller.rescued.RescuedResponse;
import xyz.teamjaune.ndi.controller.rescuer.RescuerResponse;
import xyz.teamjaune.ndi.entity.Authentication;
import xyz.teamjaune.ndi.exception.NotFoundException;
import xyz.teamjaune.ndi.service.RescueService;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping(path = "api/private/rescue")
public class RescueController {

    private RescueService rescueService;

    @PostMapping
    public ResponseEntity<RescueResponse> create(@AuthenticationPrincipal Authentication authentication,
                                                  @RequestBody RescueRequest rescueRequest) {
        try {
            RescueResponse rescueResponse = rescueService.create(rescueRequest);
            return new ResponseEntity<>(rescueResponse, HttpStatus.CREATED);
        } catch (NotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/accept/{id}")
    //@PreAuthorize("hasAnyAuthority(T" + "(xyz.teamjaune.ndi.entity.UserRole).ADMIN)")
    public ResponseEntity<RescueResponse> accept(@AuthenticationPrincipal Authentication authentication,
                                                  @PathVariable Long id) {
        try {
            return new ResponseEntity<>(rescueService.accept(id), HttpStatus.ACCEPTED);
        } catch (NotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/refuse/{id}")
    //@PreAuthorize("hasAnyAuthority(T" + "(xyz.teamjaune.ndi.entity.UserRole).ADMIN)")
    public ResponseEntity<Void> refuse(@AuthenticationPrincipal Authentication authentication,
                                       @PathVariable Long id) {
        rescueService.refuse(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/temp")
    //@PreAuthorize("hasAnyAuthority(T" + "(xyz.teamjaune.ndi.entity.UserRole).ADMIN)")
    public ResponseEntity<List<RescueResponse>> getAllTemp(@AuthenticationPrincipal Authentication authentication) {
        return new ResponseEntity<>(rescueService.getAllTemp(), HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<List<RescueResponse>> getAll(@AuthenticationPrincipal Authentication authentication) {
        return new ResponseEntity<>(rescueService.getAll(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<RescueResponse> get(@PathVariable Long id) {
        try {
            return new ResponseEntity<>(rescueService.get(id), HttpStatus.OK);
        } catch (NotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/{id}/rescued")
    public ResponseEntity<List<RescuedResponse>> getAllRescueds(@PathVariable Long id) {
        return new ResponseEntity<>(rescueService.getAllRescueds(id), HttpStatus.OK);
    }

    @GetMapping("/{id}/rescued/{idRescued}")
    public ResponseEntity<RescuedResponse> getRescued(@PathVariable Long id, @PathVariable Long idRescued) {
        try {
            return new ResponseEntity<>(rescueService.getRescued(id, idRescued), HttpStatus.OK);
        } catch (NotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/{id}/rescuer")
    public ResponseEntity<List<RescuerResponse>> getAllRescuers(@PathVariable Long id) {
        return new ResponseEntity<>(rescueService.getAllRescuers(id), HttpStatus.OK);
    }

    @GetMapping("/{id}/rescuer/{idRescuer}")
    public ResponseEntity<RescuerResponse> getRescuer(@PathVariable Long id, @PathVariable Long idRescuer) {
        try {
            return new ResponseEntity<>(rescueService.getRescuer(id, idRescuer), HttpStatus.OK);
        } catch (NotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }


}
