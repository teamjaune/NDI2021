package xyz.teamjaune.ndi.controller.rescued;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import xyz.teamjaune.ndi.entity.temp.RescuedTemp;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RescuedRequest implements Serializable {
    private Long id;
    private String firstname;
    private String lastname;
    private Date birthDate;
    private Date deathDate;

    public static RescuedTemp toTemp(RescuedRequest rescuedRequest) {
        return new RescuedTemp(
                rescuedRequest.getId(),
                rescuedRequest.getFirstname(),
                rescuedRequest.getLastname(),
                rescuedRequest.getBirthDate(),
                rescuedRequest.getDeathDate());
    }
}
