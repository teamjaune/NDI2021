package xyz.teamjaune.ndi.controller.rescuer;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import xyz.teamjaune.ndi.entity.Rescue;
import xyz.teamjaune.ndi.entity.Rescuer;
import xyz.teamjaune.ndi.entity.temp.RescuerTemp;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RescuerResponse {
    private Long id;
    private Boolean temp;
    private String firstname;
    private String lastname;
    private Date birthDate;
    private Date deathDate;
    private String job;
    private String biography;
    private String maritalStatus;
    private String genealogicalData;
    private String career;
    private List<Long> idRescues;

    public static RescuerResponse parse(Rescuer rescuer) {
        return new RescuerResponse(
                rescuer.getId(),
                false,
                rescuer.getFirstname(),
                rescuer.getLastname(),
                rescuer.getBirthDate(),
                rescuer.getDeathDate(),
                rescuer.getJob(),
                rescuer.getBiography(),
                rescuer.getMaritalStatus(),
                rescuer.getGenealogicalData(),
                rescuer.getCareer(),
                rescuer.getRescues().stream().map(Rescue::getId).collect(Collectors.toList()));
    }

    public static RescuerResponse parseTemp(RescuerTemp rescuerTemp) {
        return new RescuerResponse(
                rescuerTemp.getId(),
                true,
                rescuerTemp.getFirstname(),
                rescuerTemp.getLastname(),
                rescuerTemp.getBirthDate(),
                rescuerTemp.getDeathDate(),
                rescuerTemp.getJob(),
                rescuerTemp.getBiography(),
                rescuerTemp.getMaritalStatus(),
                rescuerTemp.getGenealogicalData(),
                rescuerTemp.getCareer(),
                new ArrayList<>());
    }

    public static List<RescuerResponse> parse(List<Rescuer> rescuers) {
        List<RescuerResponse> rescuerResponses = new ArrayList<>();
        for(Rescuer rescuer : rescuers) {
            rescuerResponses.add(RescuerResponse.parse(rescuer));
        }
        return rescuerResponses;
    }

    public static List<RescuerResponse> parseTemp(List<RescuerTemp> rescuerTemps) {
        List<RescuerResponse> rescuerResponses = new ArrayList<>();
        for(RescuerTemp rescuerTemp : rescuerTemps) {
            rescuerResponses.add(RescuerResponse.parseTemp(rescuerTemp));
        }
        return rescuerResponses;
    }
}
