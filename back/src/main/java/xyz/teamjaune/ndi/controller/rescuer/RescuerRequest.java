package xyz.teamjaune.ndi.controller.rescuer;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import xyz.teamjaune.ndi.entity.temp.RescuerTemp;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RescuerRequest implements Serializable {
    private Long id;
    private String firstname;
    private String lastname;
    private Date birthDate;
    private Date deathDate;
    private String job;
    private String biography;
    private String maritalStatus;
    private String genealogicalData;
    private String career;

    public static RescuerTemp toTemp(RescuerRequest rescuerRequest) {
        return new RescuerTemp(
                rescuerRequest.getId(),
                rescuerRequest.getFirstname(),
                rescuerRequest.getLastname(),
                rescuerRequest.getBirthDate(),
                rescuerRequest.getDeathDate(),
                rescuerRequest.getJob(),
                rescuerRequest.getBiography(),
                rescuerRequest.getMaritalStatus(),
                rescuerRequest.getGenealogicalData(),
                rescuerRequest.getCareer());
    }
}
