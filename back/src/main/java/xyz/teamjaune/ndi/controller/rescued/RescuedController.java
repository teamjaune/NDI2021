package xyz.teamjaune.ndi.controller.rescued;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import xyz.teamjaune.ndi.controller.rescue.RescueResponse;
import xyz.teamjaune.ndi.entity.Authentication;
import xyz.teamjaune.ndi.exception.NotFoundException;
import xyz.teamjaune.ndi.service.RescuedService;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping(path = "api/private/rescued")
public class RescuedController {

    private RescuedService rescuedService;

    @PostMapping
    public ResponseEntity<RescuedResponse> create(@AuthenticationPrincipal Authentication authentication,
                                                  @RequestBody RescuedRequest rescuedRequest) {
        RescuedResponse rescuedResponse = rescuedService.create(rescuedRequest);
        return new ResponseEntity<>(rescuedResponse, HttpStatus.CREATED);
    }

    @PostMapping("/accept/{id}")
    //@PreAuthorize("hasAnyAuthority(T" + "(xyz.teamjaune.ndi.entity.UserRole).ADMIN)")
    public ResponseEntity<RescuedResponse> accept(@AuthenticationPrincipal Authentication authentication,
                                                    @PathVariable Long id) {
        try {
            return new ResponseEntity<>(rescuedService.accept(id), HttpStatus.ACCEPTED);
        } catch (NotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/refuse/{id}")
    //@PreAuthorize("hasAnyAuthority(T" + "(xyz.teamjaune.ndi.entity.UserRole).ADMIN)")
    public ResponseEntity<Void> refuse(@AuthenticationPrincipal Authentication authentication,
                                       @PathVariable Long id) {
        rescuedService.refuse(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/temp")
    //@PreAuthorize("hasAnyAuthority(T" + "(xyz.teamjaune.ndi.entity.UserRole).ADMIN)")
    public ResponseEntity<List<RescuedResponse>> getAllTemp(@AuthenticationPrincipal Authentication authentication) {
        return new ResponseEntity<>(rescuedService.getAllTemp(), HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<List<RescuedResponse>> getAll(@AuthenticationPrincipal Authentication authentication) {
        return new ResponseEntity<>(rescuedService.getAll(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<RescuedResponse> get(@PathVariable Long id) {
        try {
            return new ResponseEntity<>(rescuedService.get(id), HttpStatus.OK);
        } catch (NotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/{id}/rescue")
    public ResponseEntity<List<RescueResponse>> getAllRescue(@PathVariable Long id) {
        return new ResponseEntity<>(rescuedService.getAllRescue(id), HttpStatus.OK);
    }
}
