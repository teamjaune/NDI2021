package xyz.teamjaune.ndi.controller;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.FirebaseToken;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import xyz.teamjaune.ndi.entity.User;
import xyz.teamjaune.ndi.repository.UserRepository;

import java.util.Optional;

@AllArgsConstructor
@RestController
@RequestMapping(path = "api/public/login")
public class LoginController {

    private FirebaseAuth firebaseAuth;
    private UserRepository userRepository;

    public static void firebaseTokenToUserDto(FirebaseToken decodedToken, User user) {
        user.setFirebaseId(decodedToken.getUid());
        user.setUsername(decodedToken.getName());
        user.setEmail(decodedToken.getEmail());
        user.setPicture(decodedToken.getPicture());
        user.setIssuer(decodedToken.getIssuer());
        user.setEnabled(decodedToken.isEmailVerified());
    }

    @GetMapping("/")
    public ResponseEntity<String> login(@RequestHeader("Authorization") String authHeader) {
        String token = authHeader;
        if (StringUtils.hasText(token) && token.startsWith("Bearer ")) {
            token = token.substring(7);
        }
        try {
            FirebaseToken decodedToken = firebaseAuth.verifyIdToken(token);
            Optional<User> userOptional = userRepository.findByFirebaseId(decodedToken.getUid());
            userOptional.ifPresentOrElse(ignored -> {},
                    () -> {
                        User newUser = new User();
                        firebaseTokenToUserDto(decodedToken, newUser);
                        userRepository.save(newUser);
                    }
            );
        } catch (FirebaseAuthException authException) {
            return ResponseEntity.status(401).body("Un Authorized");
        }
        return  ResponseEntity.status(200).body("OK");
    }
}
