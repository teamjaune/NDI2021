package xyz.teamjaune.ndi.controller.search;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import xyz.teamjaune.ndi.controller.rescue.RescueResponse;
import xyz.teamjaune.ndi.controller.rescued.RescuedResponse;
import xyz.teamjaune.ndi.controller.rescuer.RescuerResponse;
import xyz.teamjaune.ndi.service.RescueService;
import xyz.teamjaune.ndi.service.RescuedService;
import xyz.teamjaune.ndi.service.RescuerService;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping(path = "api/private/search")
public class SearchController {

    private RescuedService rescuedService;
    private RescuerService rescuerService;
    private RescueService rescueService;

    @PostMapping("/rescued")
    public ResponseEntity<List<RescuedResponse>> searchRescued(@RequestBody SearchRequest searchRequest) {
        return new ResponseEntity<>(rescuedService.search(searchRequest.getSearchText()), HttpStatus.OK);
    }

    @PostMapping("/rescuer")
    public ResponseEntity<List<RescuerResponse>> searchRescuer(@RequestBody SearchRequest searchRequest) {
        return new ResponseEntity<>(rescuerService.search(searchRequest.getSearchText()), HttpStatus.OK);
    }

    @PostMapping("/rescue")
    public ResponseEntity<List<RescueResponse>> searchRescue(@RequestBody SearchRequest searchRequest) {
        return new ResponseEntity<>(rescueService.search(searchRequest.getSearchText()), HttpStatus.OK);
    }
}
