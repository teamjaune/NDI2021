package xyz.teamjaune.ndi.controller.user;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import xyz.teamjaune.ndi.entity.Authentication;
import xyz.teamjaune.ndi.entity.Consumption;
import xyz.teamjaune.ndi.repository.ConsumptionRepository;

import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@AllArgsConstructor
@RestController
@RequestMapping(path = "api/private/user")
public class UserController {
    ConsumptionRepository consumptionRepository;

    @GetMapping(value="/consumption")
    public ResponseEntity<ConsumptionResponse> getConsumption(@AuthenticationPrincipal Authentication authentication){
        var values = StreamSupport.stream(consumptionRepository.findAll().spliterator(), false).map(Consumption::getAmount).collect(Collectors.toList());
        var sumTotal = values.stream().mapToInt(Integer::valueOf).filter(val -> val > 0).sum();
        var valuesUser = StreamSupport.stream(consumptionRepository.findByUser_Id(authentication.getId()).spliterator(), false).map(Consumption::getAmount).collect(Collectors.toList());
        var sumUser = valuesUser.stream().mapToInt(Integer::valueOf).filter(val -> val > 0).sum();
        return new ResponseEntity<>(new ConsumptionResponse(sumUser, sumTotal) , HttpStatus.OK);
    }
}
