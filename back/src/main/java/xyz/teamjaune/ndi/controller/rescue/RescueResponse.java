package xyz.teamjaune.ndi.controller.rescue;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import xyz.teamjaune.ndi.entity.Rescue;
import xyz.teamjaune.ndi.entity.Rescued;
import xyz.teamjaune.ndi.entity.Rescuer;
import xyz.teamjaune.ndi.entity.temp.RescueTemp;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RescueResponse {
    private Long id;
    private Boolean temp;
    private String boat;
    private Date date;
    private List<Long> rescuers = new ArrayList<>();
    private List<Long> rescueds = new ArrayList<>();

    public static RescueResponse parse(Rescue rescue) {
        return new RescueResponse(
                rescue.getId(),
                false,
                rescue.getBoat(),
                rescue.getDate(),
                rescue.getRescuers().stream().map(Rescuer::getId).collect(Collectors.toList()),
                rescue.getRescueds().stream().map(Rescued::getId).collect(Collectors.toList()));
    }

    public static RescueResponse parseTemp(RescueTemp rescueTemp) {
        return new RescueResponse(
                rescueTemp.getId(),
                true,
                rescueTemp.getBoat(),
                rescueTemp.getDate(),
                rescueTemp.getRescuers().stream().map(Rescuer::getId).collect(Collectors.toList()),
                rescueTemp.getRescueds().stream().map(Rescued::getId).collect(Collectors.toList()));
    }

    public static List<RescueResponse> parse(List<Rescue> rescues) {
        List<RescueResponse> rescueResponses = new ArrayList<>();
        for(Rescue rescue : rescues) {
            rescueResponses.add(RescueResponse.parse(rescue));
        }
        return rescueResponses;
    }

    public static List<RescueResponse> parseTemp(List<RescueTemp> rescueTemps) {
        List<RescueResponse> rescueResponses = new ArrayList<>();
        for(RescueTemp rescueTemp : rescueTemps) {
            rescueResponses.add(RescueResponse.parseTemp(rescueTemp));
        }
        return rescueResponses;
    }
}
