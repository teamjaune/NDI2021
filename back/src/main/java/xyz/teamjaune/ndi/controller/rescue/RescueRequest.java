package xyz.teamjaune.ndi.controller.rescue;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import xyz.teamjaune.ndi.entity.temp.RescueTemp;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RescueRequest implements Serializable {
    private Long id;
    private String boat;
    private Date date;
    private List<Long> rescuers = new ArrayList<>();
    private List<Long> rescueds = new ArrayList<>();

    public static RescueTemp toTemp(RescueRequest rescueRequest) {
        return new RescueTemp(
                rescueRequest.getId(),
                rescueRequest.getBoat(),
                rescueRequest.getDate(),
                new ArrayList<>(),
                new ArrayList<>());
    }
}
