package xyz.teamjaune.ndi.controller.rescuer;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import xyz.teamjaune.ndi.controller.rescue.RescueResponse;
import xyz.teamjaune.ndi.entity.Authentication;
import xyz.teamjaune.ndi.exception.NotFoundException;
import xyz.teamjaune.ndi.service.RescuerService;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping(path = "api/private/rescuer")
public class RescuerController {

    private RescuerService rescuerService;

    @PostMapping
    public ResponseEntity<RescuerResponse> create(@AuthenticationPrincipal Authentication authentication,
                                                  @RequestBody RescuerRequest rescuerRequest) {
        RescuerResponse rescuerResponse = rescuerService.create(rescuerRequest);
        return new ResponseEntity<>(rescuerResponse, HttpStatus.CREATED);
    }

    @PostMapping("/accept/{id}")
    //@PreAuthorize("hasAnyAuthority(T" + "(xyz.teamjaune.ndi.entity.UserRole).ADMIN)")
    public ResponseEntity<RescuerResponse> accept(@AuthenticationPrincipal Authentication authentication,
                                                  @PathVariable Long id) {
        try {
            return new ResponseEntity<>(rescuerService.accept(id), HttpStatus.ACCEPTED);
        } catch (NotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/refuse/{id}")
    //@PreAuthorize("hasAnyAuthority(T" + "(xyz.teamjaune.ndi.entity.UserRole).ADMIN)")
    public ResponseEntity<Void> refuse(@AuthenticationPrincipal Authentication authentication,
                       @PathVariable Long id) {
        rescuerService.refuse(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/temp")
    //@PreAuthorize("hasAnyAuthority(T" + "(xyz.teamjaune.ndi.entity.UserRole).ADMIN)")
    public ResponseEntity<List<RescuerResponse>> getAllTemp(@AuthenticationPrincipal Authentication authentication) {
        return new ResponseEntity<>(rescuerService.getAllTemp(), HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<List<RescuerResponse>> getAll(@AuthenticationPrincipal Authentication authentication) {
        return new ResponseEntity<>(rescuerService.getAll(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<RescuerResponse> get(@PathVariable Long id) {
        try {
            return new ResponseEntity<>(rescuerService.get(id), HttpStatus.OK);
        } catch (NotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/{id}/rescue")
    public ResponseEntity<List<RescueResponse>> getAllRescue(@PathVariable Long id) {
        return new ResponseEntity<>(rescuerService.getAllRescue(id), HttpStatus.OK);
    }

}
