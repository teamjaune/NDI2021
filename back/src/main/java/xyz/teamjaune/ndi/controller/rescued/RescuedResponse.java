package xyz.teamjaune.ndi.controller.rescued;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import xyz.teamjaune.ndi.entity.Rescue;
import xyz.teamjaune.ndi.entity.Rescued;
import xyz.teamjaune.ndi.entity.temp.RescuedTemp;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RescuedResponse {
    private Long id;
    private Boolean temp;
    private String firstname;
    private String lastname;
    private Date birthDate;
    private Date deathDate;
    private List<Long> idRescues;

    public static RescuedResponse parse(Rescued rescued) {
        return new RescuedResponse(
                rescued.getId(),
                false,
                rescued.getFirstname(),
                rescued.getLastname(),
                rescued.getBirthDate(),
                rescued.getDeathDate(),
                rescued.getRescues().stream().map(Rescue::getId).collect(Collectors.toList()));
    }

    public static RescuedResponse parseTemp(RescuedTemp rescuedTemp) {
        return new RescuedResponse(
                rescuedTemp.getId(),
                true,
                rescuedTemp.getFirstname(),
                rescuedTemp.getLastname(),
                rescuedTemp.getBirthDate(),
                rescuedTemp.getDeathDate(),
                new ArrayList<>());
    }

    public static List<RescuedResponse> parse(List<Rescued> rescueds) {
        List<RescuedResponse> rescuedResponses = new ArrayList<>();
        for(Rescued rescued : rescueds) {
            rescuedResponses.add(RescuedResponse.parse(rescued));
        }
        return rescuedResponses;
    }

    public static List<RescuedResponse> parseTemp(List<RescuedTemp> rescuedTemps) {
        List<RescuedResponse> rescuedResponses = new ArrayList<>();
        for(RescuedTemp rescuedTemp : rescuedTemps) {
            rescuedResponses.add(RescuedResponse.parseTemp(rescuedTemp));
        }
        return rescuedResponses;
    }
}
