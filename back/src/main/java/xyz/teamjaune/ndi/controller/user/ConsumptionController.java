package xyz.teamjaune.ndi.controller.user;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import xyz.teamjaune.ndi.controller.user.ConsumptionResponse;
import xyz.teamjaune.ndi.entity.Consumption;
import xyz.teamjaune.ndi.repository.ConsumptionRepository;

import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@AllArgsConstructor
@RestController
public class ConsumptionController {
    ConsumptionRepository consumptionRepository;
    @GetMapping(value="api/public/consumption")
    public ResponseEntity<ConsumptionResponse> getPublicConsumption(){
        var values = StreamSupport.stream(consumptionRepository.findAll().spliterator(), false).map(Consumption::getAmount).collect(Collectors.toList());
        var sumTotal = values.stream().mapToInt(Integer::valueOf).filter(val -> val > 0).sum();
        return new ResponseEntity<>(new ConsumptionResponse(0,sumTotal) , HttpStatus.OK);
    }
}