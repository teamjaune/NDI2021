package xyz.teamjaune.ndi.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import xyz.teamjaune.ndi.controller.rescue.RescueResponse;
import xyz.teamjaune.ndi.controller.rescued.RescuedRequest;
import xyz.teamjaune.ndi.controller.rescued.RescuedResponse;
import xyz.teamjaune.ndi.entity.Rescue;
import xyz.teamjaune.ndi.entity.Rescued;
import xyz.teamjaune.ndi.entity.temp.RescuedTemp;
import xyz.teamjaune.ndi.exception.NotFoundException;
import xyz.teamjaune.ndi.repository.RescueRepository;
import xyz.teamjaune.ndi.repository.RescuedRepository;
import xyz.teamjaune.ndi.repository.temp.RescuedTempRepository;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@Service
public class RescuedService {
    private static final String NOT_FOUND_RESCUED_MSG_ERROR = "Rescued with id %s was not found";
    private RescuedTempRepository rescuedTempRepository;
    private RescuedRepository rescuedRepository;
    private RescueRepository rescueRepository;

    public RescuedResponse create(RescuedRequest rescuedRequest) {
        RescuedTemp rescuedTemp = RescuedRequest.toTemp(rescuedRequest);
        rescuedTempRepository.save(rescuedTemp);
        return RescuedResponse.parseTemp(rescuedTemp);
    }

    public RescuedResponse accept(long idRescuedTemp) throws NotFoundException {
        Optional<RescuedTemp> rescuedTempOptional = rescuedTempRepository.findById(idRescuedTemp);
        if(rescuedTempOptional.isEmpty()) {
            throw new NotFoundException(String.format(NOT_FOUND_RESCUED_MSG_ERROR, idRescuedTemp));
        }
        RescuedTemp rescuedTemp = rescuedTempOptional.get();

        Rescued rescued;
        if(rescuedTemp.getIdEdit() != null) {
            Optional<Rescued> rescuedOptional = rescuedRepository.findById(rescuedTemp.getIdEdit());
            if(rescuedOptional.isEmpty()) {
                throw new NotFoundException(String.format(NOT_FOUND_RESCUED_MSG_ERROR, idRescuedTemp));
            }
            rescued = rescuedOptional.get();
            rescued.setFirstname(rescuedTemp.getFirstname());
            rescued.setLastname(rescuedTemp.getLastname());
            rescued.setBirthDate(rescuedTemp.getBirthDate());
            rescued.setDeathDate(rescuedTemp.getDeathDate());
        } else {
             rescued = new Rescued(
                     rescuedTemp.getFirstname(),
                     rescuedTemp.getLastname(),
                     rescuedTemp.getBirthDate(),
                     rescuedTemp.getDeathDate());
        }
        rescuedRepository.save(rescued);
        rescuedTempRepository.delete(rescuedTemp);
        return RescuedResponse.parse(rescued);
    }

    public void refuse(long idRescuedTemp) {
        rescuedTempRepository.deleteById(idRescuedTemp);
    }

    public List<RescuedResponse> search(String searchText) {
        List<Rescued> rescueds = rescuedRepository.searchAll(searchText);
        return RescuedResponse.parse(rescueds);
    }

    public List<RescuedResponse> getAllTemp() {
        List<RescuedTemp> rescuedTemps = (List<RescuedTemp>) rescuedTempRepository.findAll();
        return RescuedResponse.parseTemp(rescuedTemps);
    }

    public List<RescuedResponse> getAll() {
        List<Rescued> rescueds = (List<Rescued>) rescuedRepository.findAll();
        return RescuedResponse.parse(rescueds);
    }

    public RescuedResponse get(long id) throws NotFoundException {
        Optional<Rescued> rescued = rescuedRepository.findById(id);
        if(rescued.isEmpty()) {
            throw new NotFoundException(String.format(NOT_FOUND_RESCUED_MSG_ERROR, id));
        }
        return RescuedResponse.parse(rescued.get());
    }

    public List<RescueResponse> getAllRescue(long id) {
        List<Rescue> rescues = rescueRepository.findAllByRescueds_Id(id);
        return RescueResponse.parse(rescues);
    }
}
