package xyz.teamjaune.ndi.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import xyz.teamjaune.ndi.controller.rescue.RescueResponse;
import xyz.teamjaune.ndi.controller.rescuer.RescuerRequest;
import xyz.teamjaune.ndi.controller.rescuer.RescuerResponse;
import xyz.teamjaune.ndi.entity.Rescue;
import xyz.teamjaune.ndi.entity.Rescuer;
import xyz.teamjaune.ndi.entity.temp.RescuerTemp;
import xyz.teamjaune.ndi.exception.NotFoundException;
import xyz.teamjaune.ndi.repository.RescueRepository;
import xyz.teamjaune.ndi.repository.RescuerRepository;
import xyz.teamjaune.ndi.repository.temp.RescuerTempRepository;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@Service
public class RescuerService {
    private static final String NOT_FOUND_RESCUER_MSG_ERROR = "Rescuer with id %s was not found";
    private RescuerTempRepository rescuerTempRepository;
    private RescuerRepository rescuerRepository;
    private RescueRepository rescueRepository;

    public RescuerResponse create(RescuerRequest rescuerRequest) {
        RescuerTemp rescuerTemp = RescuerRequest.toTemp(rescuerRequest);
        rescuerTempRepository.save(rescuerTemp);
        return RescuerResponse.parseTemp(rescuerTemp);
    }

    public RescuerResponse accept(long idRescuerTemp) throws NotFoundException {
        Optional<RescuerTemp> rescuerTempOptional = rescuerTempRepository.findById(idRescuerTemp);
        if(rescuerTempOptional.isEmpty()) {
            throw new NotFoundException(String.format(NOT_FOUND_RESCUER_MSG_ERROR, idRescuerTemp));
        }
        RescuerTemp rescuerTemp = rescuerTempOptional.get();

        Rescuer rescuer;
        if(rescuerTemp.getIdEdit() != null) {
            Optional<Rescuer> rescuerOptional = rescuerRepository.findById(rescuerTemp.getIdEdit());
            if(rescuerOptional.isEmpty()) {
                throw new NotFoundException(String.format(NOT_FOUND_RESCUER_MSG_ERROR, idRescuerTemp));
            }
            rescuer = rescuerOptional.get();
            rescuer.setFirstname(rescuerTemp.getFirstname());
            rescuer.setLastname(rescuerTemp.getLastname());
            rescuer.setBirthDate(rescuerTemp.getBirthDate());
            rescuer.setDeathDate(rescuerTemp.getDeathDate());
            rescuer.setJob(rescuerTemp.getJob());
            rescuer.setBiography(rescuerTemp.getBiography());
            rescuer.setMaritalStatus(rescuerTemp.getMaritalStatus());
            rescuer.setCareer(rescuerTemp.getCareer());
        } else {
            rescuer = new Rescuer(
                    rescuerTemp.getFirstname(),
                    rescuerTemp.getLastname(),
                    rescuerTemp.getBirthDate(),
                    rescuerTemp.getDeathDate(),
                    rescuerTemp.getJob(),
                    rescuerTemp.getBiography(),
                    rescuerTemp.getMaritalStatus(),
                    rescuerTemp.getGenealogicalData(),
                    rescuerTemp.getCareer());
        }
        rescuerRepository.save(rescuer);
        rescuerTempRepository.delete(rescuerTemp);

        return RescuerResponse.parse(rescuer);
    }

    public void refuse(long idRescuerTemp) {
        rescuerTempRepository.deleteById(idRescuerTemp);
    }

    public List<RescuerResponse> search(String searchText) {
        List<Rescuer> rescuers = rescuerRepository.searchAll(searchText);
        return RescuerResponse.parse(rescuers);
    }

    public List<RescuerResponse> getAllTemp() {
        List<RescuerTemp> rescuerTemps = (List<RescuerTemp>) rescuerTempRepository.findAll();
        return RescuerResponse.parseTemp(rescuerTemps);
    }

    public List<RescuerResponse> getAll() {
        List<Rescuer> rescuers = (List<Rescuer>) rescuerRepository.findAll();
        return RescuerResponse.parse(rescuers);
    }

    public RescuerResponse get(long id) throws NotFoundException {
        Optional<Rescuer> rescuer = rescuerRepository.findById(id);
        if(rescuer.isEmpty()) {
            throw new NotFoundException(String.format(NOT_FOUND_RESCUER_MSG_ERROR, id));
        }
        return RescuerResponse.parse(rescuer.get());
    }

    public List<RescueResponse> getAllRescue(long id) {
        List<Rescue> rescues = rescueRepository.findAllByRescuers_Id(id);
        return RescueResponse.parse(rescues);
    }
}
