package xyz.teamjaune.ndi.service;

import lombok.AllArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import xyz.teamjaune.ndi.entity.Authentication;
import xyz.teamjaune.ndi.entity.Consumption;
import xyz.teamjaune.ndi.repository.ConsumptionRepository;
import xyz.teamjaune.ndi.repository.UserRepository;

@AllArgsConstructor
@Service
public class ConsumptionService {
    private ConsumptionRepository consumptionRepository;
    private UserRepository userRepository;
    public void create(Integer consumption) {
        var authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null) {
            var currentPrincipalName = (Authentication) authentication.getPrincipal();

        if (currentPrincipalName != null) {
            var optUser = userRepository.findByFirebaseId(currentPrincipalName.getFirebaseId());
            if (optUser.isPresent()) {
                var realConsumption = consumption < 0 ? 1 : consumption;
                consumptionRepository.save(new Consumption(realConsumption, optUser.get()));
                return;
            }
        }
    }
        consumptionRepository.save(new Consumption(consumption, null));
    }
}
