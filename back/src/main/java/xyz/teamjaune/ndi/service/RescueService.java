package xyz.teamjaune.ndi.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import xyz.teamjaune.ndi.controller.rescue.RescueRequest;
import xyz.teamjaune.ndi.controller.rescue.RescueResponse;
import xyz.teamjaune.ndi.controller.rescued.RescuedResponse;
import xyz.teamjaune.ndi.controller.rescuer.RescuerResponse;
import xyz.teamjaune.ndi.entity.Rescue;
import xyz.teamjaune.ndi.entity.Rescued;
import xyz.teamjaune.ndi.entity.Rescuer;
import xyz.teamjaune.ndi.entity.temp.RescueTemp;
import xyz.teamjaune.ndi.exception.NotFoundException;
import xyz.teamjaune.ndi.repository.RescueRepository;
import xyz.teamjaune.ndi.repository.RescuedRepository;
import xyz.teamjaune.ndi.repository.RescuerRepository;
import xyz.teamjaune.ndi.repository.temp.RescueTempRepository;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@Service
public class RescueService {
    private static final String NOT_FOUND_RESCUE_MSG_ERROR = "Rescue with id %s was not found";
    private static final String NOT_FOUND_RESCUED_IN_RECUE_MSG_ERROR = "Rescued with id %s was not found in Rescue with id %s";
    private static final String NOT_FOUND_RESCUER_IN_RECUE_MSG_ERROR = "Rescuer with id %s was not found in Rescue with id %s";
    private RescueRepository rescueRepository;
    private RescuedRepository rescuedRepository;
    private RescuerRepository rescuerRepository;
    private RescueTempRepository rescueTempRepository;

    public RescueResponse create(RescueRequest rescueRequest) throws NotFoundException {
        RescueTemp rescueTemp = RescueRequest.toTemp(rescueRequest);
        for(Long idRescued : rescueRequest.getRescueds()) {
            Optional<Rescued> rescuedOptional = rescuedRepository.findById(idRescued);
            if(rescuedOptional.isEmpty()) {
                throw new NotFoundException(String.format(NOT_FOUND_RESCUED_IN_RECUE_MSG_ERROR, idRescued, rescueTemp.getId()));
            }
            rescueTemp.getRescueds().add(rescuedOptional.get());
        }
        for(Long idRescuer : rescueRequest.getRescuers()) {
            Optional<Rescuer> rescuerOptional = rescuerRepository.findById(idRescuer);
            if(rescuerOptional.isEmpty()) {
                throw new NotFoundException(String.format(NOT_FOUND_RESCUER_IN_RECUE_MSG_ERROR, idRescuer, rescueTemp.getId()));
            }
            rescueTemp.getRescuers().add(rescuerOptional.get());
        }
        rescueTempRepository.save(rescueTemp);
        return RescueResponse.parseTemp(rescueTemp);
    }

    public RescueResponse accept(long idRescueTemp) throws NotFoundException {
        Optional<RescueTemp> rescueTempOptional = rescueTempRepository.findById(idRescueTemp);
        if(rescueTempOptional.isEmpty()) {
            throw new NotFoundException("");
        }
        RescueTemp rescueTemp = rescueTempOptional.get();

        Rescue rescue;
        if(rescueTemp.getIdEdit() != null) {
            Optional<Rescue> rescueOptional = rescueRepository.findById(rescueTemp.getIdEdit());
            if(rescueOptional.isEmpty()) {
                throw new NotFoundException("");
            }
            rescue = rescueOptional.get();
            rescue.setDate(rescueTemp.getDate());
            rescue.setBoat(rescueTemp.getBoat());
            rescue.setRescueds(rescueTemp.getRescueds());
            rescue.setRescuers(rescueTemp.getRescuers());
        } else {
            rescue = new Rescue(
                    rescueTemp.getBoat(),
                    rescueTemp.getDate());
        }
        rescue.getRescueds().addAll(rescueTemp.getRescueds());
        rescue.getRescuers().addAll(rescueTemp.getRescuers());
        for(Rescued rescued : rescueTemp.getRescueds()) {
            rescued.getRescues().add(rescue);
        }
        for(Rescuer rescuer : rescueTemp.getRescuers()) {
            rescuer.getRescues().add(rescue);
        }
        rescueRepository.save(rescue);
        rescueTempRepository.delete(rescueTemp);

        return RescueResponse.parse(rescue);
    }

    public void refuse(long idRescueTemp) {
        rescueTempRepository.deleteById(idRescueTemp);
    }

    public List<RescueResponse> search(String searchText) {
        List<Rescue> rescues = rescueRepository.searchAll(searchText);
        return RescueResponse.parse(rescues);
    }

    public List<RescueResponse> getAllTemp() {
        List<RescueTemp> rescueTemps = (List<RescueTemp>) rescueTempRepository.findAll();
        return RescueResponse.parseTemp(rescueTemps);
    }

    public List<RescueResponse> getAll() {
        List<Rescue> rescues = (List<Rescue>) rescueRepository.findAll();
        return RescueResponse.parse(rescues);
    }

    public RescueResponse get(long id) throws NotFoundException {
        Optional<Rescue> rescue = rescueRepository.findById(id);
        if(rescue.isEmpty()) {
            throw new NotFoundException(String.format(NOT_FOUND_RESCUE_MSG_ERROR, id));
        }
        return RescueResponse.parse(rescue.get());
    }

    public List<RescuedResponse> getAllRescueds(long id) {
        List<Rescued> rescueds = rescuedRepository.findAllByRescues_Id(id);
        return RescuedResponse.parse(rescueds);
    }

    public RescuedResponse getRescued(long id, long idRescued) throws NotFoundException {
        Optional<Rescued> rescued = rescuedRepository.findByIdAndRescues_Id(idRescued, id);
        if(rescued.isEmpty()) {
            throw new NotFoundException(String.format(NOT_FOUND_RESCUED_IN_RECUE_MSG_ERROR, idRescued, id));
        }
        return RescuedResponse.parse(rescued.get());
    }

    public List<RescuerResponse> getAllRescuers(long id) {
        List<Rescuer> rescuers = rescuerRepository.findAllByRescues_Id(id);
        return RescuerResponse.parse(rescuers);
    }

    public RescuerResponse getRescuer(long id, long idRescuer) throws NotFoundException {
        Optional<Rescuer> rescuer = rescuerRepository.findByIdAndRescues_Id(idRescuer, id);
        if(rescuer.isEmpty()) {
            throw new NotFoundException(String.format(NOT_FOUND_RESCUER_IN_RECUE_MSG_ERROR, idRescuer, id));
        }
        return RescuerResponse.parse(rescuer.get());
    }
}
