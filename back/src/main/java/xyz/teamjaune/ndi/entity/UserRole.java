package xyz.teamjaune.ndi.entity;

public enum UserRole {
    USER,
    ADMIN
}
