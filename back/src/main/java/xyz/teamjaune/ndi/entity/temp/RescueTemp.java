package xyz.teamjaune.ndi.entity.temp;

import lombok.*;
import xyz.teamjaune.ndi.entity.Rescued;
import xyz.teamjaune.ndi.entity.Rescuer;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@ToString(onlyExplicitlyIncluded = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class RescueTemp {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "resue_id")
    private Long id;

    @Column(name = "rescue_id_edit")
    private Long idEdit;

    @Column(name = "rescue_boat")
    private String boat;

    @Column(name = "rescue_date")
    private Date date;

    @ManyToMany
    public List<Rescuer> rescuers = new ArrayList<>();

    @ManyToMany
    public List<Rescued> rescueds = new ArrayList<>();

    public RescueTemp(Long idEdit, String boat, Date date, List<Rescuer> rescuers, List<Rescued> rescueds) {
        this.idEdit = idEdit;
        this.boat = boat;
        this.date = date;
        this.rescuers = rescuers;
        this.rescueds = rescueds;
    }
}
