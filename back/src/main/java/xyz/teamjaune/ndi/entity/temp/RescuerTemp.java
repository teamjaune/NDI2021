package xyz.teamjaune.ndi.entity.temp;

import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@ToString(onlyExplicitlyIncluded = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class RescuerTemp {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "rescuer_id")
    private Long id;

    @Column(name = "rescuer_id_edit")
    private Long idEdit;

    @Column(name = "rescuer_firstname")
    private String firstname;

    @Column(name = "rescuer_lastname")
    private String lastname;

    @Column(name = "rescuer_birthdate")
    private Date birthDate;

    @Column(name = "rescuer_deathDate")
    private Date deathDate;

    @Column(name = "rescuer_job")
    private String job;

    @Column(name = "rescuer_biography")
    private String biography;

    @Column(name = "rescuer_maritalStatus")
    private String maritalStatus;

    @Column(name = "rescuer_genealogicalData")
    private String genealogicalData;

    @Column(name = "rescuer_career")
    private String career;

    // TODO actions ?
    // TODO nbRescue to compute
    // TODO nbPeopleSaved to compute


    public RescuerTemp(Long idEdit, String firstname, String lastname, Date birthDate, Date deathDate, String job, String biography, String maritalStatus, String genealogicalData, String career) {
        this.idEdit = idEdit;
        this.firstname = firstname;
        this.lastname = lastname;
        this.birthDate = birthDate;
        this.deathDate = deathDate;
        this.job = job;
        this.biography = biography;
        this.maritalStatus = maritalStatus;
        this.genealogicalData = genealogicalData;
        this.career = career;
    }
}
