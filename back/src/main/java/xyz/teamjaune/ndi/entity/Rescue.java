package xyz.teamjaune.ndi.entity;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@ToString(onlyExplicitlyIncluded = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Rescue {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "resue_id")
    private Long id;

    @Column(name = "rescue_boat")
    private String boat;

    @Column(name = "rescue_date")
    private Date date;

    @ManyToMany
    public List<Rescuer> rescuers = new ArrayList<>();

    @ManyToMany
    public List<Rescued> rescueds = new ArrayList<>();

    public Rescue(String boat, Date date) {
        this.boat = boat;
        this.date = date;
    }
}
