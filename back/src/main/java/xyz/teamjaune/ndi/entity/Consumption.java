package xyz.teamjaune.ndi.entity;

import lombok.*;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@ToString(onlyExplicitlyIncluded = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Consumption {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "consumption_id")
    private Long id;

    @Column(name = "consumption_amount")
    private Integer amount;

    @CreationTimestamp
    @Temporal(value = TemporalType.TIMESTAMP)
    @Column(name = "consumption_date")
    private Date measurementTime;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "user_user_id")
    private User user;

    public Consumption(Integer amount, User user) {
        this.amount = amount;
        this.user = user;
    }
}
