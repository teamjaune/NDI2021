package xyz.teamjaune.ndi.entity.temp;

import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@ToString(onlyExplicitlyIncluded = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class RescuedTemp {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "rescued_id")
    private Long id;

    @Column(name = "rescued_id_edit")
    private Long idEdit;

    @Column(name = "rescued_firstname")
    private String firstname;

    @Column(name = "rescued_lastname")
    private String lastname;

    @Column(name = "rescued_birthDate")
    private Date birthDate;

    @Column(name = "rescued_deathDate")
    private Date deathDate;

    public RescuedTemp(Long idEdit, String firstname, String lastname, Date birthDate, Date deathDate) {
        this.idEdit = idEdit;
        this.firstname = firstname;
        this.lastname = lastname;
        this.birthDate = birthDate;
        this.deathDate = deathDate;
    }
}
