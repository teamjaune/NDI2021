package xyz.teamjaune.ndi.entity;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@ToString(onlyExplicitlyIncluded = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Rescued {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "rescued_id")
    private Long id;

    @Column(name = "rescued_firstname")
    private String firstname;

    @Column(name = "rescued_lastname")
    private String lastname;

    @Column(name = "rescued_birthDate")
    private Date birthDate;

    @Column(name = "rescued_deathDate")
    private Date deathDate;

    @ManyToMany
    private List<Rescue> rescues = new ArrayList<>();

    public Rescued(String firstname, String lastname, Date birthDate, Date deathDate) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.birthDate = birthDate;
        this.deathDate = deathDate;
    }
}
