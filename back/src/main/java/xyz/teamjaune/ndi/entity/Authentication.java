package xyz.teamjaune.ndi.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Authentication {
    private Long id;
    private UserRole role;
    private String firebaseId;
    private String username;
    private String email;

    public static Authentication parse(User user) {
        return new Authentication(
                user.getId(),
                user.getRole(),
                user.getFirebaseId(),
                user.getUsername(),
                user.getEmail());
    }
}
