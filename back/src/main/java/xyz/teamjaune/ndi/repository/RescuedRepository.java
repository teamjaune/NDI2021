package xyz.teamjaune.ndi.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import xyz.teamjaune.ndi.entity.Rescued;

import java.util.List;
import java.util.Optional;

@Repository
public interface RescuedRepository extends CrudRepository<Rescued, Long> {

    List<Rescued> findAllByRescues_Id(Long id);

    Optional<Rescued> findByIdAndRescues_Id(Long id, Long idRescue);

    @Query(value = "SELECT r FROM Rescued r WHERE concat(lower(r.lastname), ' ', lower(r.firstname)) LIKE lower(concat('%', :searchText,'%')) OR concat(lower(r.firstname), ' ', lower(r.lastname)) LIKE lower(concat('%', :searchText,'%'))")
    List<Rescued> searchAll(String searchText);
}
