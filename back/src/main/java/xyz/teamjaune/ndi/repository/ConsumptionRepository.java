package xyz.teamjaune.ndi.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import xyz.teamjaune.ndi.entity.Consumption;

import java.util.List;

@Repository
public interface ConsumptionRepository extends CrudRepository<Consumption, Long> {
    List<Consumption> findByUser_Id(Long id);

}
