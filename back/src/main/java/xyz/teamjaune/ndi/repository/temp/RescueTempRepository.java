package xyz.teamjaune.ndi.repository.temp;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import xyz.teamjaune.ndi.entity.temp.RescueTemp;

@Repository
public interface RescueTempRepository extends CrudRepository<RescueTemp, Long> {

}
