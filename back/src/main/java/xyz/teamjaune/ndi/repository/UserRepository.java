package xyz.teamjaune.ndi.repository;

import lombok.NonNull;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import xyz.teamjaune.ndi.entity.User;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {
    @NonNull
    List<User> findAll();


    Optional<User> findById(long id);

    Optional<User> findByFirebaseId(String firebaseId);

    @Query(value = "SELECT u FROM User u WHERE lower(u.username) LIKE lower(concat('%', :searchText,'%')) OR lower(u.email) LIKE lower(concat('%', :searchText,'%'))")
    List<User> searchAll(String searchText, Pageable pageable);
}
