package xyz.teamjaune.ndi.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import xyz.teamjaune.ndi.entity.Rescue;

import java.util.List;

@Repository
public interface RescueRepository extends CrudRepository<Rescue, Long> {

    List<Rescue> findAllByRescuers_Id(Long id);

    List<Rescue> findAllByRescueds_Id(Long id);

    @Query(value = "SELECT r FROM Rescue r WHERE lower(r.boat) LIKE lower(concat('%', :searchText,'%'))")
    List<Rescue> searchAll(String searchText);
}
