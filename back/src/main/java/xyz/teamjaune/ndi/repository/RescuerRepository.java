package xyz.teamjaune.ndi.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import xyz.teamjaune.ndi.entity.Rescuer;

import java.util.List;
import java.util.Optional;

@Repository
public interface RescuerRepository extends CrudRepository<Rescuer, Long> {


    List<Rescuer> findAllByRescues_Id(Long id);

    Optional<Rescuer> findByIdAndRescues_Id(Long id, Long idRescue);

    @Query(value = "SELECT r FROM Rescuer r WHERE concat(lower(r.lastname), ' ', lower(r.firstname)) LIKE lower(concat('%', :searchText,'%')) OR concat(lower(r.firstname), ' ', lower(r.lastname)) LIKE lower(concat('%', :searchText,'%'))")
    List<Rescuer> searchAll(String searchText);

}
