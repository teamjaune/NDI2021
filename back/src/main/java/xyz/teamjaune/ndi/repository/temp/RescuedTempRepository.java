package xyz.teamjaune.ndi.repository.temp;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import xyz.teamjaune.ndi.entity.temp.RescuedTemp;

@Repository
public interface RescuedTempRepository extends CrudRepository<RescuedTemp, Long> {

}
