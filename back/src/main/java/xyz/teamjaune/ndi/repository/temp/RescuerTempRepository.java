package xyz.teamjaune.ndi.repository.temp;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import xyz.teamjaune.ndi.entity.temp.RescuerTemp;

@Repository
public interface RescuerTempRepository extends CrudRepository<RescuerTemp, Long> {

}
