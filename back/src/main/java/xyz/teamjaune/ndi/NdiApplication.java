package xyz.teamjaune.ndi;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.auth.FirebaseAuth;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.Resource;

import java.io.IOException;

@SpringBootApplication
public class NdiApplication {

	@Value("classpath:ndi2021-firebase-adminsdk.json")
	Resource serviceAccount;

	public static void main(String[] args) {
		SpringApplication.run(NdiApplication.class, args);
	}

	@Bean
	public FirebaseAuth firebaseAuth() throws IOException {
		FirebaseOptions.Builder options = FirebaseOptions.builder();
		options.setCredentials(GoogleCredentials.fromStream(serviceAccount.getURL().openStream()));
		FirebaseApp.initializeApp(options.build());
		return FirebaseAuth.getInstance();
	}
}
