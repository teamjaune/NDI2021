package xyz.teamjaune.ndi.exception;

public class NotFoundException extends Exception {
    public NotFoundException(String e) {
        super(e);
    }
}
