#!/bin/bash
mvn -f /app/pom.xml clean spring-boot:run -Dmaven.test.skip -Dspring-boot.run.arguments="--spring.datasource.url=jdbc:mysql://mysql/certify_db?useSSL=false"
