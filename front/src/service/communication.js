
const sendGetRequest = (endPoint, token) => {
    const requestOptions = {
        method: 'GET',
        headers: { 'Content-Type': 'application/json', 'Authorization': `Bearer ${token}` }
    };
    return fetch(`${process.env.REACT_APP_SERVER_ADDRESS}/${endPoint}`, requestOptions);
}

const sendPostRequest = (endPoint, token, object) => {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json', 'Authorization': `Bearer ${token}` },
        body: JSON.stringify(object)
    };
    return fetch(`${process.env.REACT_APP_SERVER_ADDRESS}/${endPoint}`, requestOptions);
}

const sendPutRequest = (endPoint, token, object) => {
    const requestOptions = {
        method: 'Put',
        headers: { 'Content-Type': 'application/json', 'Authorization': `Bearer ${token}` },
        body: JSON.stringify(object)
    };
    return fetch(`${process.env.REACT_APP_SERVER_ADDRESS}/${endPoint}`, requestOptions);
}

const sendDeleteRequest = (endPoint, token, id) => {
    const requestOptions = {
        method: 'Delete',
        headers: { 'Authorization': `Bearer ${token}` }
    };
    return fetch(`${process.env.REACT_APP_SERVER_ADDRESS}/${endPoint}/${id}`, requestOptions);
}

export default { sendGetRequest, sendPostRequest, sendPutRequest, sendDeleteRequest }
