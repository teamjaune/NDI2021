import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import commonEn from './Translations/en/common.json';
import commonFr from './Translations/fr/common.json';
import commonPirate from './Translations/pirate/common.json';

export const getDefaultLanguage = () => {
  if (!localStorage.getItem('language')) {
    localStorage.setItem('language', 'fr');
  }
  return localStorage.getItem('language');
}

i18n
  .use(initReactI18next)
  .init({
    lng: getDefaultLanguage(),
    resources: {
      en: {
        common: commonEn
      },
      fr: {
        common: commonFr
      },
      pirate: {
        common: commonPirate
      }
    },
    interpolation: {
      escapeValue: false
    }
  });
  export default i18n;
