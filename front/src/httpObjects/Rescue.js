class Rescue {
    constructor(id, boat, date, rescuers, rescueds) {
        this.id = id;
        this.boat = boat;
        this.date = date;
        this.rescuers = rescuers;
        this.rescueds = rescueds;
    }
}

export default Rescue;
