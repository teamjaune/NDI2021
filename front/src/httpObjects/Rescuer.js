class Rescuer {
    constructor(id, lastname, firstname, birthDate, deathDate) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.birthDate = birthDate;
        this.deathDate = deathDate;
        this.job = null;
        this.biography = null;
        this.maritalStatus = null;
        this.genealogicalData = null;
        this.career = null;
    }
}

export default Rescuer;
