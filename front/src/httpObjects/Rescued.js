class Rescued {
    constructor(id, lastname, firstname, birthDate, deathDate) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.birthDate = birthDate;
        this.deathDate = deathDate;
    }
}

export default Rescued;
