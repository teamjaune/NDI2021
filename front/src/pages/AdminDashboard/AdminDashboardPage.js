import * as React from 'react';
import * as Mui from '@mui/material';
import RescuedElement from './RescuedElement';
import RescueElement from './RescueElement';
import Communication from '../../service/communication';
import { getAuth } from 'firebase/auth';
import { useTranslation } from 'react-i18next';

export default function AdminDashboardPage() {
  const [rescuer, setRescuer] = React.useState([]);
  const [rescued, setRescued] = React.useState([]);
  const [rescue, setRescue] = React.useState([]);

  React.useEffect(() => {
    getAuth().currentUser.getIdToken().then(token => {
      Communication.sendGetRequest('api/private/rescuer/temp', token).then(async (response) => {
        const result = await response.json();
        if (result === null) {
          setRescuer([]);
        } else {
          if (result) {
            setRescuer(result);
          }
        }
      }).catch((_) => {

      });
      Communication.sendGetRequest('api/private/rescued/temp', token).then(async (response) => {
        const result = await response.json();
        if (result === null) {
          setRescued([]);
        } else {
          if (result) {
            setRescued(result);
          }
        }
      }).catch((_) => {

      });
      Communication.sendGetRequest('api/private/rescue/temp', token).then(async (response) => {
        const result = await response.json();
        if (result === null) {
          setRescue([]);
        } else {
          if (result) {
            setRescue(result);
          }
        }
      }).catch((_) => {

      });
    });
  }, []);

  const [t] = useTranslation('common');

  return (
    <>
      <Mui.Stack direction="row"
        justifyContent="space-evenly"
        alignItems="center"
      >
        <Mui.Stack direction="column"
          justifyContent="space-evenly"
          alignItems="center"
        >
          <Mui.Typography variant='h4'>{t('navbar.tableauDeBord')}</Mui.Typography>
          <Mui.Stack direction="column"
            justifyContent="space-evenly"
            alignItems="center"
          >
            <Mui.Typography variant='h4'>{t('person.sauvetages')}</Mui.Typography>
            {rescue && rescue.map((r) => <RescueElement key={r.id} rescue={r}></RescueElement>)}
          </Mui.Stack>
          <Mui.Stack direction="column"
            justifyContent="space-evenly"
            alignItems="center"
          >
            <Mui.Typography variant='h4'>{t('person.sauveteurs')}</Mui.Typography>
            {rescuer && rescuer.map((r) => <RescuedElement api='rescuer' key={r.id} rescued={r}></RescuedElement>)}
          </Mui.Stack>
          <Mui.Stack direction="column"
            justifyContent="space-evenly"
            alignItems="center"
          >
            <Mui.Typography variant='h4'>{t('person.sauves')}</Mui.Typography>
            {rescued && rescued.map((r) => <RescuedElement api='rescued' key={r.id} rescued={r}></RescuedElement>)}
          </Mui.Stack>
        </Mui.Stack>
      </Mui.Stack>
    </>
  );
}
