import * as React from 'react';
import * as Mui from '@mui/material';
import Communication from '../../service/communication';
import { getAuth } from 'firebase/auth';
import { useTranslation } from 'react-i18next';

export default function RescueElement(props) {
    const [display, setDisplay] = React.useState('block');

    const accept = () => {
        if (getAuth().currentUser != null) {
            getAuth().currentUser.getIdToken().then(token => {
                Communication.sendPostRequest('api/private/rescue/accept/' + props.rescue.id, token);
            });
            setDisplay('none');
        }
    }
    const decline = () => {
        if (getAuth().currentUser != null) {
            getAuth().currentUser.getIdToken().then(token => {
                Communication.sendPostRequest('api/private/rescue/refuse/' + props.rescue.id, token);
            });
            setDisplay('none');
        }
    }

    const [t] = useTranslation('common');

    return (
        <Mui.Card style={{ display: display }}>
            <Mui.CardContent>
                <Mui.Typography align="center" gutterBottom variant="h6" component="div">
                    {props.rescue.boat}
                </Mui.Typography>
                <Mui.Typography align="center" gutterBottom variant="h6" component="div">
                    {new Date(props.rescue.date).toLocaleString().split(',')[0]}
                </Mui.Typography>
            </Mui.CardContent>
            <Mui.CardActions>
                <Mui.Button variant="contained" color="success" onClick={accept}>
                    {t('person.accepter')}
                </Mui.Button>
                <Mui.Button variant="contained" color="error" onClick={decline}>
                    {t('person.refuser')}
                </Mui.Button>
            </Mui.CardActions>
        </Mui.Card>
    );
}
