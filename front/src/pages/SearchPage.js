import * as React from 'react';
import * as Mui from '@mui/material';
import { useParams } from 'react-router-dom';
import { getAuth } from 'firebase/auth';
import Communication from '../service/communication';
import SearchText from '../httpObjects/SearchText';
import SearchRescuerTab from '../components/Search/SearchRescuerTab';
import SearchRescuedTab from '../components/Search/SearchRescuedTab';
import SearchRescueTab from '../components/Search/SearchRescueTab';
import { useTranslation } from 'react-i18next';

export default function SearchPage() {
    const { searchText } = useParams();
    const [value, setValue] = React.useState(0);

    const [loadingRescued, setLoadingRescued] = React.useState(true);
    const [loadingRescuer, setLoadingRescuer] = React.useState(true);
    const [loadingRescue, setLoadingRescue] = React.useState(true);

    const [rescued, setRescued] = React.useState([]);
    const [rescuer, setRescuer] = React.useState([]);
    const [rescue, setRescue] = React.useState([]);

    const [searchTextSave, setSearchTextSave] = React.useState('');

    const handleChange = (_, newValue) => {
        setValue(newValue);
    };

    React.useEffect(() => {
        if (searchText !== searchTextSave) {
            setSearchTextSave(searchText);

            setRescued([]);
            setLoadingRescued(true);
            fetchRescued();

            setRescuer([]);
            setLoadingRescuer(true);
            fetchRescuer();

            setRescue([]);
            setLoadingRescue(true);
            fetchRescue();
        }
    }, [searchText, loadingRescued, loadingRescuer, loadingRescue]);

    function TabPanel(props) {
        const { children, value, index, ...other } = props;

        return (
            <div
                role="tabpanel"
                hidden={value !== index}
                id={`simple-tabpanel-${index}`}
                aria-labelledby={`simple-tab-${index}`}
                {...other}
            >
                {value === index && (
                    <Mui.Box sx={{ p: 3 }}>
                        <Mui.Typography>{children}</Mui.Typography>
                    </Mui.Box>
                )}
            </div>
        );
    }

    const fetchRescued = () => {
        setLoadingRescued(true);
        getAuth().currentUser.getIdToken().then(token => {
            const searchRequest = new SearchText(searchText);
            Communication.sendPostRequest('api/private/search/rescued', token, searchRequest).then(async (response) => {
                const result = await response.json();
                if (result === null || !Array.isArray(result)) {
                    setRescued([]);
                    setLoadingRescued(false);
                } else {
                    setRescued(result);
                    setLoadingRescued(false);
                }
            }).catch((_) => {
                setRescued(undefined);
                setLoadingRescued(false);
            });
        });
    };

    const fetchRescuer = () => {
        setLoadingRescuer(true);
        getAuth().currentUser.getIdToken().then(token => {
            const searchRequest = new SearchText(searchText);
            Communication.sendPostRequest('api/private/search/rescuer', token, searchRequest).then(async (response) => {
                const result = await response.json();
                if (result === null || !Array.isArray(result)) {
                    setRescuer([]);
                    setLoadingRescuer(false);
                } else {
                    setRescuer(result);
                    setLoadingRescuer(false);
                }
            }).catch((_) => {
                setRescuer(undefined);
                setLoadingRescuer(false);
            });
        });
    };

    const fetchRescue = () => {
        setLoadingRescue(true);
        getAuth().currentUser.getIdToken().then(token => {
            const searchRequest = new SearchText(searchText);
            Communication.sendPostRequest('api/private/search/rescue', token, searchRequest).then(async (response) => {
                const result = await response.json();
                if (result === null || !Array.isArray(result)) {
                    setRescue([]);
                    setLoadingRescue(false);
                } else {
                    setRescue(result);
                    setLoadingRescue(false);
                }
            }).catch((_) => {
                setRescue(undefined);
                setLoadingRescue(false);
            });
        });
    };

    const [t] = useTranslation('common');

    return (
        <>
            <Mui.Box sx={{ width: '100%' }}>
                <Mui.Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                    <Mui.Tabs value={value} onChange={handleChange} centered>
                        <Mui.Tab label={t('person.sauvetages')} />
                        <Mui.Tab label={t('person.sauveteurs')} />
                        <Mui.Tab label={t('person.sauves')} />
                    </Mui.Tabs>
                </Mui.Box>
                <TabPanel value={value} index={0}>
                    <SearchRescueTab rescue={rescue} loading={loadingRescue} setLoading={setLoadingRescue}></SearchRescueTab>
                </TabPanel>
                <TabPanel value={value} index={1}>
                    <SearchRescuerTab rescuer={rescuer} loading={loadingRescuer} setLoading={setLoadingRescuer}></SearchRescuerTab>
                </TabPanel>
                <TabPanel value={value} index={2}>
                    <SearchRescuedTab rescued={rescued} loading={loadingRescued} setLoading={setLoadingRescued}></SearchRescuedTab>
                </TabPanel>
                <Mui.Grid
                    container
                    direction="row"
                    justifyContent="space-evenly"
                    alignItems="center"
                >
                    {value === 0 && loadingRescue && <Mui.CircularProgress />}
                    {value === 1 && loadingRescuer && <Mui.CircularProgress />}
                    {value === 2 && loadingRescued && <Mui.CircularProgress />}
                </Mui.Grid>
            </Mui.Box>
        </>
    );
}
