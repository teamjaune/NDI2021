import * as React from 'react';
import * as Mui from '@mui/material';
import Communication from '../service/communication';
import { styled } from '@mui/material/styles';
import { useParams, useNavigate } from 'react-router-dom';
import { getAuth } from 'firebase/auth';
import { useTranslation } from 'react-i18next';

export default function RescuerPage() {
    const navigate = useNavigate();

    const { id } = useParams();
    const [rescuer, setRescuer] = React.useState(null);
    const [rescues, setRescues] = React.useState(null);

    const Typography = styled(Mui.Typography)(({ _ }) => ({
        '&:hover': {
            'cursor': 'pointer',
            'textDecoration': 'underline'
        }
    }));

    React.useEffect(() => {
        if (!id) {
            return;
        }
        getAuth().currentUser.getIdToken().then(token => {
            Communication.sendGetRequest('api/private/rescuer/' + id, token).then(async (response) => {
                const result = await response.json();
                setRescuer(result);
            }).catch((_) => {

            });
            Communication.sendGetRequest('api/private/rescuer/' + id + '/rescue', token).then(async (response) => {
                const result = await response.json();
                setRescues(result);
            }).catch((_) => {

            });
        });
    }, [id]);

    const [t] = useTranslation('common');

    return (
        <>
            {!rescuer && <Mui.CircularProgress />}
            {rescuer && <>
                <Mui.Typography variant='h3'>{t('person.sauveteur')}</Mui.Typography>
                <Mui.Typography variant='h4'>{rescuer.lastname + ' ' + rescuer.firstname}</Mui.Typography>
                <Mui.Typography variant='h4'>{rescuer.birthDate.toLocaleString().split('T')[0] + ' -- ' + rescuer.deathDate.toLocaleString().split('T')[0]}</Mui.Typography>
            </>}
            <Mui.Typography variant='h5'>{t('person.sauvetages')}</Mui.Typography>
            {!rescues && <Mui.Typography variant='subtitle2'>{t('person.pasDeSauvetages')}</Mui.Typography>}
            {rescues && rescues.map((r) => rescues.map(function (r) {
                return <Typography key={r.id} onClick={() => navigate('/Rescue/' + r.id)} variant='subtitle1'>{r.boat}</Typography>
            }))}
        </>
    );
}
