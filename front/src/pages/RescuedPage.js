import * as React from 'react';
import * as Mui from '@mui/material';
import Communication from '../service/communication';
import { styled } from '@mui/material/styles';
import { useParams, useNavigate } from 'react-router-dom';
import { getAuth } from 'firebase/auth';
import { useTranslation } from 'react-i18next';

export default function RescuedPage() {
    const navigate = useNavigate();

    const { id } = useParams();
    const [rescued, setRescued] = React.useState(null);
    const [rescues, setRescues] = React.useState(null);

    const Typography = styled(Mui.Typography)(({ _ }) => ({
        '&:hover': {
            'cursor': 'pointer',
            'textDecoration': 'underline'
        }
    }));

    const Card = styled(Mui.Card)(({ _ }) => ({
        'height': '100%',
        'marginLeft': '5px',
        'marginRight': '5px',
        'display': 'flex',
        'justifyContent': 'space-between',
        'flexDirection': 'column',
        'backgroundColor': '#FAFAFA',
    }));

    React.useEffect(() => {
        if (!id) {
            return;
        }
        getAuth().currentUser.getIdToken().then(token => {
            Communication.sendGetRequest('api/private/rescued/' + id, token).then(async (response) => {
                const result = await response.json();
                setRescued(result);
            }).catch((_) => {

            });
            Communication.sendGetRequest('api/private/rescued/' + id + '/rescue', token).then(async (response) => {
                const result = await response.json();
                console.log(result);
                setRescues(result);
            }).catch((_) => {

            });
        });
    }, [id]);

    const [t] = useTranslation('common');

    return (
    <Card>
        {!rescued && <Mui.CircularProgress />}
        {rescued && <Mui.Box sx = {{ display: 'flex', flexDirection: 'row;' }}>
            <Mui.CardMedia sx = {{ height: '25%', width: '25%'}}
                component="img"
                image= '../img/profile.png'/>
            <Mui.CardContent>
                <Mui.Typography variant='h3'>{t('person.sauve')}</Mui.Typography>
                <Mui.Typography variant='h4'>{rescued.lastname + ' ' + rescued.firstname}</Mui.Typography>
                <Mui.Typography variant='h4'>{rescued.birthDate.toLocaleString().split('T')[0] + ' -- ' + rescued.deathDate.toLocaleString().split('T')[0]}</Mui.Typography>
            </Mui.CardContent>
            </Mui.Box>}
        <Mui.CardActions>
            <Mui.Typography variant='h5'>{t('person.sauvetages')}</Mui.Typography>
            {!rescues && <Mui.Typography variant='subtitle2'>{t('person.pasDeSauvetages')}</Mui.Typography>}
            {rescues && 
            <Mui.Stack direction="row"
                    justifyContent="space-evenly"
                    sx={{ width: '100%' }}
                    alignItems="center">
                {rescues.map((r) => rescues.map(function (r) {
                    return <Typography key={r.id} onClick={() => navigate('/Rescue/' + r.id)} variant='subtitle1'>{r.boat}</Typography>
                }))}
            </Mui.Stack>}
        </Mui.CardActions>
    </Card> 
    );
}
