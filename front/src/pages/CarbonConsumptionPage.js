import { getAuth } from '@firebase/auth';
import * as React from 'react';
import * as Mui from '@mui/material'
import * as MuiIcons from '@mui/icons-material';
import GridContainer from '../components/CarbonConsumption/GridContainer';
import GridItem from '../components/CarbonConsumption/GridItem';
import Card from '../components/CarbonConsumption/Card';
import CardHeader from '../components/CarbonConsumption/CardHeader';
import CardIcon from '../components/CarbonConsumption/CardIcon';
import CardFooter from '../components/CarbonConsumption/CardFooter';
import { makeStyles } from '@mui/styles';
import styles from '../components/CarbonConsumption/style/dashboardStyle.js';
import communication from '../service/communication';
import { useTranslation } from 'react-i18next';

const useStyles = makeStyles(styles);

export default function CarbonConsumption() {
  const [userConsumption, setUserConsumption] = React.useState(0);
  const [totalConsumption, setTotalConsumption] = React.useState(0);
  const [isLoading, setIsLoading] = React.useState(true);

  async function updateCounter() {
    setIsLoading(true);
    if (!getAuth().currentUser) {
      getAuth().currentUser.getIdToken().then((token) => {
        communication.sendGetRequest('api/public/consumption', token)
        .then(async (response) => {
          const result = await response.json();
          setUserConsumption(Math.round((result.userConsumption * (7.20 * 10 ** -11 + 4.88 * 10 ** -10) * 460 + 1.16 * 3.095 * 10 ** -4 * 59) * 1000 * 100) / 1000);
          setTotalConsumption(Math.round((result.totalConsumption * (7.20 * 10 ** -11 + 4.88 * 10 ** -10) * 460 + 1.16 * 3.095 * 10 ** -4 * 59) * 1000 * 100) / 1000);
          setIsLoading(false);
        });
      });
    } else {
      getAuth().currentUser.getIdToken().then((token) => {
        communication.sendGetRequest('api/private/user/consumption', token)
        .then(async (response) => {
          const result = await response.json();
          setUserConsumption(Math.round((result.userConsumption * (7.20 * 10 ** -11 + 4.88 * 10 ** -10) * 460 + 1.16 * 3.095 * 10 ** -4 * 59) * 1000 * 100) / 1000);
          setTotalConsumption(Math.round((result.totalConsumption * (7.20 * 10 ** -11 + 4.88 * 10 ** -10) * 460 + 1.16 * 3.095 * 10 ** -4 * 59) * 1000 * 100) / 1000);
          setIsLoading(false);
        });
      });
    }
  }

  React.useEffect(() => {
    updateCounter();
    setInterval(updateCounter, 10000);
  }, []);

  const classes = useStyles();
  const [t] = useTranslation('common');

  return (
    <Mui.Box>
      <Mui.Typography
        variant='h3'
        align='center'
      >
        {t('carbon.titre')}
      </Mui.Typography>
      <Mui.Typography
        variant='h5'
        align='center'
      >
        {t('carbon.votreEmpreinte')}
      </Mui.Typography>
      <GridContainer justifyContent='center'>
        <GridItem xs={12} sm={6} md={3}>
          <Card>
            <CardHeader color='info' stats icon>
            <CardIcon color='info'>
                <MuiIcons.Accessibility />
              </CardIcon>
              <p className={classes.cardCategory}>{t('carbon.bilanCarbone')}</p>
              <h3 className={classes.cardTitle}>
                { !isLoading && userConsumption } { isLoading && <Mui.CircularProgress /> }
                { !isLoading && 'gCO2' }
              </h3>
            </CardHeader>
            <CardFooter stats>
              <div className={classes.stats}>
                <MuiIcons.Update />
                {t('carbon.aJour')}
              </div>
            </CardFooter>
          </Card>
        </GridItem>
        <GridItem xs={12} sm={6} md={3}>
          <Card>
            <CardHeader color='success' stats icon>
              <CardIcon color='success'>
                <MuiIcons.Yard />
              </CardIcon>
              <p className={classes.cardCategory}>{t('carbon.bilanCarboneGlobal')}</p>
              <h3 className={classes.cardTitle}>
                { !isLoading && totalConsumption } { isLoading && <Mui.CircularProgress /> }
                { !isLoading && 'gCO2' }
              </h3>
            </CardHeader>
            <CardFooter stats>
              <div className={classes.stats}>
                <MuiIcons.Update />
                {t('carbon.aJour')}
              </div>
            </CardFooter>
          </Card>
        </GridItem>
      </GridContainer>
      <Mui.Box
        sx={{
          margin: 'auto',
          width: '60%'
        }}
      >

        <Mui.CssBaseline/>
        <Mui.Typography
          variant='h4'
          align='center'
          m={2}
        >
          {t('carbon.questCeQuePollution')}
        </Mui.Typography>
        <Mui.Typography
          mx={4}
        >
          {t('carbon.explicationPollution')}
          <Mui.Link href='https://blog.link-value.fr/impact-carbone-du-numerique-mesure-emissions-du-web-9bb348a6410a'>
            {t('carbon.ici')}
          </Mui.Link>.
        </Mui.Typography>
        <Mui.Typography
          variant='h4'
          align='center'
          m={2}
        >
          {t('carbon.titreBonnesPratiques')}
        </Mui.Typography>
        <Mui.Typography
          mx={4}
        >
          {t('carbon.bonnesPratiques')}
        </Mui.Typography>
      </Mui.Box>
    </Mui.Box>
  );
}
