import * as React from 'react';
import Box from '@mui/material/Box';
import * as Mui from '@mui/material'
import { styled } from '@mui/material/styles';
import { useTranslation } from 'react-i18next';

export default function MainPage() {
  const PersonLink = styled(Mui.Link)(({ _ }) => ({
    'color': '#000000',
    'textDecoration': 'none',
    '&:hover': {
      'color': '#000000',
      'textDecoration': 'none'
    }
  }));

  const [t] = useTranslation('common');

  return (
    <>
      <img style={{ margin: '2% 5% 2% 5%', width: '90%' }} src="../img/banner.PNG" />
      <Box
        sx={{
          display: 'flex',
          flexWrap: 'wrap',
          margin: '2% 5% 0 5%'
        }}
      >
        <Mui.Typography variant='h5' style={{ maxWidth: '50%', marginRight: '2%', textAlign: 'justify' }}>{t('home.bienvenue')}</Mui.Typography>
        <Mui.Typography variant='h5' sx={{ maxWidth: '48%', textAlign: 'center', margin: 'auto', fontWeight: 'bold' }}>{t('home.citation')}<br /><PersonLink variant='h6' sx={{ color: 'black', textDecoration: 'none', float: 'right' }}>Arsène Bossu</PersonLink> </Mui.Typography>
        <img style={{ margin: '2% auto 2% auto', maxWidth: '30%' }} src="../img/interview.jpg" />
        <img style={{ margin: '2% auto 2% auto', width: '30%' }} src="../img/Corsaires.jpg" />
        <Mui.Typography variant='h5' style={{ maxWidth: '50%', marginRight: '2%', textAlign: 'center' }}> <Mui.Typography variant='h5' style={{ textWeight: 'bold', color: '#aa0e03', marginBottom: '2%' }}> {t('home.coinInfo')} </Mui.Typography> <Mui.Typography variant='h5' style={{ textWeight: 'bold', fontStyle: 'italic', marginBottom: '2%' }}> {t('home.pasDIdeePourNoel')} </Mui.Typography> <Mui.Typography variant='h6' style={{ marginBottom: '2%' }}> {t('home.boutiqueSNSM')} </Mui.Typography> <Mui.Typography variant='h5' style={{ textWeight: 'bold', marginBottom: '2%' }}> {t('home.horairesOuverture')} </Mui.Typography> <Mui.Typography variant='h5' style={{ textWeight: 'bold' }}> {t('home.porteOuverteNoel')} </Mui.Typography> <Mui.Typography variant='h6'> {t('home.horairesPorteOuverteNoel')} </Mui.Typography></Mui.Typography>
        <img style={{ maxWidth: '48%', margin: 'auto' }} src="../img/boutique.jpg" />
        <div>
          <img style={{ width: '100%' }} src='../img/logos/logo.png'></img>
        </div>
      </Box>
    </>
  );
}
