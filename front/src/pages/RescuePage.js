import * as React from 'react';
import * as Mui from '@mui/material';
import Communication from '../service/communication';
import { styled } from '@mui/material/styles';
import { useParams, useNavigate } from 'react-router-dom';
import { getAuth } from 'firebase/auth';
import { useTranslation } from 'react-i18next';

export default function RescuePage() {
    const navigate = useNavigate();

    const { id } = useParams();
    const [rescue, setRescue] = React.useState(null);
    const [rescuer, setRescuer] = React.useState(null);
    const [rescued, setRescued] = React.useState(null);

    const Typography = styled(Mui.Typography)(({ _ }) => ({
        '&:hover': {
            'cursor': 'pointer',
            'textDecoration': 'underline'
        }
    }));

    React.useEffect(() => {
        if (!id) {
            return;
        }
        getAuth().currentUser.getIdToken().then(token => {
            Communication.sendGetRequest('api/private/rescue/' + id, token).then(async (response) => {
                const result = await response.json();
                setRescue(result);
            }).catch((_) => {

            });
            Communication.sendGetRequest('api/private/rescue/' + id + '/rescuer', token).then(async (response) => {
                const result = await response.json();
                setRescuer(result);
            }).catch((_) => {

            });
            Communication.sendGetRequest('api/private/rescue/' + id + '/rescued', token).then(async (response) => {
                const result = await response.json();
                setRescued(result);
            }).catch((_) => {

            });
        });
    }, [id]);

    const [t] = useTranslation('common');

    return (
        <>
            {!rescue && <Mui.CircularProgress />}
            {rescue && <>
                <Mui.Typography variant='h3'>{t('person.sauvetage')}</Mui.Typography>
                <Mui.Typography variant='h4'>{rescue.boat}</Mui.Typography>
                <Mui.Typography variant='h4'>{rescue.date.toLocaleString().split('T')[0]}</Mui.Typography>
            </>}
            <Mui.Typography variant='h5'>{t('person.sauveteurs')}</Mui.Typography>
            {!rescuer && <Mui.Typography variant='subtitle2'>{t('person.pasDeSauveteurs')}</Mui.Typography>}
            {rescuer && rescuer.map((r) => rescuer.map(function (r) {
                return <Typography key={r.id} onClick={() => navigate('/Rescuer/' + r.id)} variant='subtitle1'>{r.lastname + ' ' + r.firstname}</Typography>
            }))}
            <Mui.Typography variant='h5'>{t('person.sauves')}</Mui.Typography>
            {!rescued && <Mui.Typography variant='subtitle2'>{t('person.pasDeSauves')}</Mui.Typography>}
            {rescued && rescued.map((r) => rescued.map(function (r) {
                return <Typography key={r.id} onClick={() => navigate('/Rescued/' + r.id)} variant='subtitle1'>{r.lastname + ' ' + r.firstname}</Typography>
            }))}
        </>
    );
}
