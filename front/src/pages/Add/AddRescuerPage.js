import * as React from 'react';
import * as Mui from '@mui/material';
import AddRescuer from '../../components/Add/AddRescuer';
import { useTranslation } from 'react-i18next';

export default function AddRescuerPage() {
  const [t] = useTranslation('common');

  return (
    <>
      <Mui.Stack direction="row"
        justifyContent="space-evenly"
        alignItems="center"
      >
        <Mui.Stack direction="column"
          justifyContent="space-evenly"
          alignItems="center"
        >
          <Mui.Typography variant='h4'>{t('add.sauveteur')}</Mui.Typography>
          <AddRescuer></AddRescuer>
        </Mui.Stack>
      </Mui.Stack>
    </>
  );
}
