import * as React from 'react';
import * as Mui from '@mui/material';
import AddRescued from '../../components/Add/AddRescued';
import { useTranslation } from 'react-i18next';

export default function AddRescuedPage() {
  const [t] = useTranslation('common');

  return (
    <>
      <Mui.Stack direction="row"
        justifyContent="space-evenly"
        alignItems="center"
      >
        <Mui.Stack direction="column"
          justifyContent="space-evenly"
          alignItems="center"
        >
          <Mui.Typography variant='h4'>{t('add.sauve')}</Mui.Typography>
          <AddRescued></AddRescued>
        </Mui.Stack>
      </Mui.Stack>
    </>
  );
}
