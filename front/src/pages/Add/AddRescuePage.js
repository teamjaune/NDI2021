import * as React from 'react';
import * as Mui from '@mui/material';
import * as MuiIcons from '@mui/icons-material';
import { styled } from '@mui/material/styles';
import { getAuth } from 'firebase/auth';
import SearchText from '../../httpObjects/SearchText';
import Communication from '../../service/communication';
import Rescue from '../../httpObjects/Rescue';
import { useTranslation } from 'react-i18next';

export default function AddRescuePage() {
  const [boatName, setBoatName] = React.useState(null);
  const [date, setDate] = React.useState(null);

  const [loadingRescuer, setLoadingRescuer] = React.useState(true);
  const [rescuer, setRescuer] = React.useState([]);

  const [loadingRescued, setLoadingRescued] = React.useState(true);
  const [rescued, setRescued] = React.useState([]);

  const [chosenRescuer, setChosenRescuer] = React.useState([]);
  const [chosenRescued, setChosenRescued] = React.useState([]);

  const [success, setSuccess] = React.useState(false);
  const [error, setError] = React.useState(false);

  const Typography = styled(Mui.Typography)(({ _ }) => ({
    '&:hover': {
      'cursor': 'pointer',
      'textDecoration': 'underline'
    }
  }));

  const send = () => {
    if (!boatName) {
      setError(true);
      return;
    }
    getAuth().currentUser.getIdToken().then(token => {
      const dbDate = new Date(date);
      const rescue = new Rescue(null, boatName, dbDate.toISOString(), chosenRescuer, chosenRescued);
      Communication.sendPostRequest('api/private/rescue', token, rescue).then(async (response) => {
        const result = await response.json();
        if (result === null) {
          setSuccess(false);
        } else {
          setSuccess(true);
        }
      }).catch((_) => {

      });
    });
  };

  const searchRescuers = (searchText) => {
    setRescuer([]);
    setLoadingRescuer(true);
    fetchRescuer(searchText);
  };

  const searchRescued = (searchText) => {
    setRescued([]);
    setLoadingRescued(true);
    fetchRescued(searchText);
  };

  const fetchRescued = (searchText) => {
    setLoadingRescued(true);
    getAuth().currentUser.getIdToken().then(token => {
      const searchRequest = new SearchText(searchText);
      Communication.sendPostRequest('api/private/search/rescued', token, searchRequest).then(async (response) => {
        const result = await response.json();
        if (result === null || !Array.isArray(result)) {
          setRescued([]);
          setLoadingRescued(false);
        } else {
          setRescued(result);
          setLoadingRescued(false);
        }
      }).catch((_) => {
        setRescued(undefined);
        setLoadingRescued(false);
      });
    });
  };

  const fetchRescuer = (searchText) => {
    setLoadingRescuer(true);
    getAuth().currentUser.getIdToken().then(token => {
      const searchRequest = new SearchText(searchText);
      Communication.sendPostRequest('api/private/search/rescuer', token, searchRequest).then(async (response) => {
        const result = await response.json();
        if (result === null || !Array.isArray(result)) {
          setRescuer([]);
          setLoadingRescuer(false);
        } else {
          setRescuer(result);
          setLoadingRescuer(false);
        }
      }).catch((_) => {
        setRescuer(undefined);
        setLoadingRescuer(false);
      });
    });
  };

  const chooseRescuer = (r) => {
    const chosen = chosenRescuer;
    for (const c of chosen) {
      if (c === r.id) {
        return;
      }
    }
    chosen.push(r.id);
    setChosenRescuer(chosen);
  };

  const chooseRescued = (r) => {
    const chosen = chosenRescued;
    for (const c of chosen) {
      if (c === r.id) {
        return;
      }
    }
    chosen.push(r.id);
    setChosenRescued(chosen);
  };

  const [t] = useTranslation('common');

  return (
    <>
      <Mui.Stack direction='row'
        justifyContent='space-evenly'
        alignItems='center'
      >
        <Mui.Stack direction='column'
          justifyContent='space-evenly'
          alignItems='center'
        >
          <Mui.Typography variant='h4'>{t('add.sauvetage')}</Mui.Typography>
          <Mui.TextField id='outlined-basic' label={t('add.nomBateau')} variant='outlined' onChange={(e) => setBoatName(e.target.value)} />
          <br />
          <Mui.TextField
            id='date'
            type='date'
            label='Date'
            onChange={(e) => setDate(e.target.value)}
            InputLabelProps={{
              shrink: true
            }}
          />

          <br />

          <Mui.Typography variant='h5'>{t('add.rescuers')}</Mui.Typography>
          <Mui.TextField id='outlined-basic' label={t('ajouter.nom')} variant='outlined' onChange={(e) => searchRescuers(e.target.value)} />

          {loadingRescuer && <Mui.CircularProgress />}
          {!loadingRescuer && <>
            <Mui.Stack direction='row'
              justifyContent='space-evenly'
              alignItems='center'
            >
              {rescuer.map((r) => <Typography key={r.id} style={{ marginLeft: '3px' }} onClick={() => chooseRescuer(r)} variant='subtitle2'>{r.lastname + ' ' + r.firstname}</Typography>)}
            </Mui.Stack>
          </>}

          <br />

          <Mui.Typography variant='h5'>{t('add.rescueds')}</Mui.Typography>
          <Mui.TextField id='outlined-basic' label={t('ajouter.nom')} variant='outlined' onChange={(e) => searchRescued(e.target.value)} />

          {loadingRescued && <Mui.CircularProgress />}
          {!loadingRescued && <>
            <Mui.Stack direction='row'
              justifyContent='space-evenly'
              alignItems='center'
            >
              {rescued.map((r) => <Typography key={r.id} style={{ marginLeft: '3px' }} onClick={() => chooseRescued(r)} variant='subtitle2'>{r.lastname + ' ' + r.firstname}</Typography>)}
            </Mui.Stack>
          </>}
          <br />
          <Mui.Button variant='contained' onClick={send}>{t('ajouter.add')}</Mui.Button>
          {success &&
            <Mui.Alert
              severity='success'
              action={
                <Mui.IconButton
                  aria-label='close'
                  color='inherit'
                  size='small'
                  onClick={() => {
                    setSuccess(false);
                  }}
                >
                  <MuiIcons.Close fontSize='inherit' />
                </Mui.IconButton>
              }
            >
              <strong>{t('add.sauvetageSucces')}</strong>
            </Mui.Alert>
          }
          {error &&
            <Mui.Alert
              severity='error'
              action={
                <Mui.IconButton
                  aria-label='close'
                  color='inherit'
                  size='small'
                  onClick={() => {
                    setError(false);
                  }}
                >
                  <MuiIcons.Close fontSize='inherit' />
                </Mui.IconButton>
              }
            >
              <strong>{t('ajouter.erreur')}</strong>
            </Mui.Alert>
          }
        </Mui.Stack>
      </Mui.Stack>
    </>
  );
}
