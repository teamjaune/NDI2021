import * as React from 'react';
import * as Mui from '@mui/material'
import * as MuiIcons from '@mui/icons-material';
import SignInComponent from '../components/Login/SignIn';
import SignUpComponent from '../components/Login/SignUp';
import WelcomeMessage from '../components/Login/WelcomeMessage';
import { getAuth, signInWithPopup, GoogleAuthProvider } from 'firebase/auth';
import GoogleButton from 'react-google-button';
import { ErrorMessages, ErrorType } from '../components/Login/ErrorMessage';
import { useTranslation } from 'react-i18next';

const googleProvider = new GoogleAuthProvider();
const theme = Mui.createTheme();

export default function LoginPage() {
  const [login, setLogin] = React.useState(true);
  const [errorMessage, setErrorMessage] = React.useState(ErrorMessages.NO_ERROR);

  const switchLogin = () => {
    setLogin(!login);
  };

  const [t] = useTranslation('common');

  return (
    <Mui.ThemeProvider theme={theme}>
      <Mui.CssBaseline />
      <Mui.Grid container component='main' sx={{ height: '100vh' }}>
        <Mui.Grid
          item
          xs={false}
          sm={8}
          md={5}
          sx={{
            backgroundColor: (t) =>
              t.palette.mode === 'light' ? t.palette.grey[50] : t.palette.grey[900],
            backgroundImage: `url(${process.env.PUBLIC_URL + '/images/login_background_image.jpg'})`,
            backgroundRepeat: 'no-repeat',
            backgroundSize: 'cover',
            backgroundPosition: 'center'
          }}
        >
          {WelcomeMessage(login)}
        </Mui.Grid>
        <Mui.Grid item xs={12}
          sm={4}
          md={7}
          component={Mui.Paper}
          elevation={6}
          square
        >
          <Mui.Box
            sx={{
              my: 8,
              mx: 6,
              height: '80%',
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center'
            }}
          >
            <Mui.Box style={{ width: '60%' }}>
              <Mui.Typography component='h1' variant='h3' sx={{ paddingBottom: 1 }}>
                { login ? 'Sign In' : 'Sign Up' }
              </Mui.Typography>
            </Mui.Box>
            {errorMessage.type !== ErrorType.NO_ERROR &&
              <Mui.Box sx={{ py: 1 }}>
                <Mui.Alert
                  severity='error'
                  action={
                    <Mui.IconButton
                      aria-label='close'
                      color='inherit'
                      size='small'
                      onClick={() => {
                        setErrorMessage(ErrorMessages.NO_ERROR);
                      }}
                    >
                      <MuiIcons.Close fontSize='inherit' />
                    </Mui.IconButton>
                  }
                >
                  <strong>Error:</strong> {errorMessage.message}
                </Mui.Alert>
              </Mui.Box>
            }
            {login ? <SignInComponent /> : <SignUpComponent />}
            <Mui.Box sx={{ alignSelf: 'center', p: 1 }}>
              <GoogleButton
                type="dark" // can be light or dark
                onClick={() => {
                  signInWithPopup(getAuth(), googleProvider)
                  .catch(error => {
                    switch (error.code) {
                      case 'auth/popup-closed-by-user':
                        setErrorMessage(ErrorMessages.POPUP_CLOSE);
                        break;
                      case 'auth/popup-blocked':
                        setErrorMessage(ErrorMessages.POPUP_BLOCK);
                        break;
                      default:
                        setErrorMessage(ErrorMessages.UNKNOWN_ERROR);
                        break;
                    }
                  });
                }
                }
              />
            </Mui.Box>
            <Mui.Box sx={{ alignSelf: 'center', p: 3 }}>
              <Mui.Grid container sx={{ alignItems: 'center' }}>
                <Mui.Grid item>
                  <Mui.Button fullWidth
                    aria-label='outlined primary'
                    variant='outlined'
                    disabled={login}
                    onClick={switchLogin}
                  >
                    {t('login.authentifier')}
                  </Mui.Button>
                </Mui.Grid>
                <Mui.Grid item>
                  <Mui.Typography variant='button' color='text.secondary' align='center' marginLeft='10px' marginRight='10px'>
                    {t('login.ou')}
                  </Mui.Typography>
                </Mui.Grid>
                <Mui.Grid item>
                  <Mui.Button fullWidth
                    aria-label='outlined primary'
                    variant='outlined'
                    disabled={!login}
                    onClick={switchLogin}
                  >
                    {t('login.inscrire')}
                  </Mui.Button>
                </Mui.Grid>
              </Mui.Grid>
            </Mui.Box>
            <Mui.Typography variant='body2' color='text.secondary' align='center' style={{ marginTop: 'auto' }}>
              Copyright © TeamJaune {new Date().getFullYear()}.
            </Mui.Typography>
          </Mui.Box>
        </Mui.Grid>
      </Mui.Grid>
    </Mui.ThemeProvider>
  );
}
