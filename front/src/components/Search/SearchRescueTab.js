import * as React from 'react';
import * as Mui from '@mui/material';
import { useNavigate } from 'react-router-dom';
import { styled } from '@mui/material/styles';
import { useTranslation } from 'react-i18next';

export default function SearchRescueTab(props) {
    const navigate = useNavigate();

    const Typography = styled(Mui.Typography)(({ _ }) => ({
        '&:hover': {
            'cursor': 'pointer',
            'textDecoration': 'underline'
        }
    }));

    const Card = styled(Mui.Card)(({ _ }) => ({
        'width': '80%',
        'marginBottom': '20px',
        'display': 'flex',
        'backgroundColor': '#FAFAFA',
        '.title': {
            'overflow': 'hidden',
            'textOverflow': 'ellipsis',
            'display': '-webkit-box',
            '-webkit-line-clamp': '1',
            '-webkit-box-orient': 'vertical'
        },
        '.synopsis': {
            'overflow': 'hidden',
            'textOverflow': 'ellipsis',
            'display': '-webkit-box',
            '-webkit-line-clamp': '2',
            '-webkit-box-orient': 'vertical'
        },
        '.hover:hover': {
            'cursor': 'pointer',
            'textDecoration': 'underline'
        }
    }));

    const displayRescue = () => {
        if (props.loading && props.rescue.length <= 0) {
            return <></>;
        }
        if (props.rescue === undefined) {
            return <Mui.Typography variant='h5'>{t('search.serverError')}</Mui.Typography>;
        }
        if (props.rescue.length <= 0) {
            return <Mui.Typography variant='h5'>{t('search.noResults')}</Mui.Typography>;
        }
        return props.rescue.map(function (r) {
            return <Card key={r.id}>
                <Mui.CardMedia className='hover' sx={{ height: '200px', width: 'auto' }}
                    component='img'
                    image="/img/rescue.png"
                />
                <Mui.Box sx={{ display: 'flex', flexDirection: 'column', height: '200px' }}>
                    <Mui.CardContent>
                        <Typography key={r.id}  onClick={() => navigate('/Rescue/' + r.id)}  align='left' gutterBottom variant='subtitle1' component='div'>
                            {r.boat}
                        </Typography>
                    </Mui.CardContent>
                </Mui.Box>
            </Card>
        });
    };

    const [t] = useTranslation('common');

    return (
        <Mui.Stack
            direction="column"
            justifyContent="center"
            sx={{ width: '100%' }}
            alignItems="center"
        >
            {displayRescue()}
        </Mui.Stack>
    );
}
