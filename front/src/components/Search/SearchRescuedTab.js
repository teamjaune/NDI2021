import * as React from 'react';
import * as Mui from '@mui/material';
import { useNavigate } from 'react-router-dom';
import { styled } from '@mui/material/styles';
import { useTranslation } from 'react-i18next';

export default function SearchRescuedTab(props) {
    const navigate = useNavigate();

    const Typography = styled(Mui.Typography)(({ _ }) => ({
        '&:hover': {
            'cursor': 'pointer',
            'textDecoration': 'underline'
        }
    }));

    const Card = styled(Mui.Card)(({ _ }) => ({
        'width': '80%',
        'marginBottom': '20px',
        'display': 'flex',
        'backgroundColor': '#FAFAFA',
        '.title': {
            'overflow': 'hidden',
            'textOverflow': 'ellipsis',
            'display': '-webkit-box',
            '-webkit-line-clamp': '1',
            '-webkit-box-orient': 'vertical'
        },
        '.synopsis': {
            'overflow': 'hidden',
            'textOverflow': 'ellipsis',
            'display': '-webkit-box',
            '-webkit-line-clamp': '2',
            '-webkit-box-orient': 'vertical'
        },
        '.hover:hover': {
            'cursor': 'pointer',
            'textDecoration': 'underline'
        }
    }));

    const displayRescued = () => {
        if (props.loading && props.rescued.length <= 0) {
            return <></>;
        }
        if (props.rescued === undefined) {
            return <Mui.Typography variant='h5'>{t('search.serverError')}</Mui.Typography>;
        }
        if (props.rescued.length <= 0) {
            return <Mui.Typography variant='h5'>{t('search.noResults')}</Mui.Typography>;
        }
        return props.rescued.map(function (r) {
            return <Card key={r.id}>
                <Mui.CardMedia className='hover' sx={{ height: '200px', width: 'auto' }}
                                component='img'
                                image="/img/profile.png"
                            />
                <Mui.Box sx={{ display: 'flex', flexDirection: 'column', height: '200px' }}>
                        <Mui.CardContent>
                            <Typography onClick={() => navigate('/Rescued/' + r.id)}  key={r.id} align='left' gutterBottom variant='subtitle1' component='div'>
                                {r.lastname + ' ' + r.firstname}
                            </Typography>
                        </Mui.CardContent>
                    </Mui.Box>
                </Card>
        });
    };

    const [t] = useTranslation('common');

    return (
        <Mui.Stack
            direction='column'
            justifyContent='center'
            sx={{ width: '100%' }}
            alignItems='center'
        >
            {displayRescued()}
        </Mui.Stack>
    );
}
