import * as React from 'react';
import * as Mui from '@mui/material';
import * as MuiIcons from '@mui/icons-material';
import Communication from '../../service/communication';
import { getAuth } from 'firebase/auth';
import Rescuer from '../../httpObjects/Rescuer';
import { useTranslation } from 'react-i18next';

export default function AddRescuer() {
  const [lastName, setLastName] = React.useState(null);
  const [firstName, setFirstName] = React.useState(null);
  const [dateBirth, setDateBirth] = React.useState(null);
  const [dateDeath, setDateDeath] = React.useState(null);

  const [success, setSuccess] = React.useState(false);
  const [error, setError] = React.useState(false);

  const send = () => {
    if (!lastName || !firstName) {
      setError(true);
      return;
    }
    getAuth().currentUser.getIdToken().then(token => {
      const dbDateBirth = new Date(dateBirth);
      const dbDateDeath = new Date(dateDeath);
      const rescued = new Rescuer(null, lastName, firstName, dbDateBirth.toISOString(), dbDateDeath.toISOString());
      Communication.sendPostRequest('api/private/rescuer', token, rescued).then(async (response) => {
        const result = await response.json();
        if (result === null) {
          setSuccess(false);
        } else {
          setSuccess(true);
        }
      }).catch((_) => {

      });
    });
  };

  const [t] = useTranslation('common');

  return (
    <>
      <Mui.Stack
        component="form"
        sx={{
          width: '25ch'
        }}
        spacing={2}
        noValidate
        autoComplete="off"
      >
        <Mui.TextField id="outlined-basic" label={t('ajouter.nom')} variant="outlined" onChange={(e) => setLastName(e.target.value)} />
        <Mui.TextField id="outlined-basic" label={t('ajouter.prenom')} variant="outlined" onChange={(e) => setFirstName(e.target.value)} />
        <Mui.TextField
          id="date"
          type="date"
          label={t('ajouter.dateDeNaissance')}
          onChange={(e) => setDateBirth(e.target.value)}
          InputLabelProps={{
            shrink: true
          }}
        />
        <Mui.TextField
          id="date"
          type="date"
          label={t('ajouter.dateDeDeces')}
          onChange={(e) => setDateDeath(e.target.value)}
          InputLabelProps={{
            shrink: true
          }}
        />
        <Mui.Button variant="contained" onClick={send}>{t('ajouter.add')}</Mui.Button>
        {success &&
          <Mui.Alert
            severity='success'
            action={
              <Mui.IconButton
                aria-label='close'
                color='inherit'
                size='small'
                onClick={() => {
                  setSuccess(false);
                }}
              >
                <MuiIcons.Close fontSize='inherit' />
              </Mui.IconButton>
            }
          >
            <strong>{t('ajouter.sauveurSucces')}</strong>
          </Mui.Alert>
        }
        {error &&
          <Mui.Alert
            severity='error'
            action={
              <Mui.IconButton
                aria-label='close'
                color='inherit'
                size='small'
                onClick={() => {
                  setError(false);
                }}
              >
                <MuiIcons.Close fontSize='inherit' />
              </Mui.IconButton>
            }
          >
            <strong>{t('ajouter.erreur')}</strong>
          </Mui.Alert>
        }
      </Mui.Stack>
    </>
  );
}
