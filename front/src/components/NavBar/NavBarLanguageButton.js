import * as React from 'react';
import * as Mui from '@mui/material';
import Flags from 'country-flag-icons/react/3x2'
import { useTranslation } from 'react-i18next';

export default function NavBarLanguageButton() {
    const { i18n } = useTranslation('common');

    const [anchorEl, setAnchorEl] = React.useState(null);

    const handleMenu = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const english = () => {
        i18n.changeLanguage('en');
        localStorage.setItem('language', 'en');
        handleClose();
    };

    const pirate = () => {
        i18n.changeLanguage('pirate');
        localStorage.setItem('language', 'pirate');
        handleClose();
    };

    const french = () => {
        i18n.changeLanguage('fr');
        localStorage.setItem('language', 'fr');
        handleClose();
    };

    return (
        <>
            <Mui.IconButton
                size="large"
                aria-label="account of current user"
                aria-controls="menu-appbar"
                aria-haspopup="true"
                onClick={handleMenu}
                color="inherit"
            >
                {localStorage.getItem('language') === 'fr' && <Flags.FR title="France" style={{ width: '25px', height: '25px' }} />}
                {localStorage.getItem('language') === 'en' && <Flags.US title="United States" style={{ width: '25px', height: '25px' }} />}
                {localStorage.getItem('language') === 'pirate' && <Flags.IO title="Pirate" style={{ width: '25px', height: '25px' }} />}
            </Mui.IconButton>
            <Mui.Menu
                id="menu-appbar"
                anchorEl={anchorEl}
                anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'right'
                }}
                sx={{ my: 4 }}
                keepMounted
                transformOrigin={{
                    vertical: 'top',
                    horizontal: 'right'
                }}
                open={anchorEl !== null}
                onClose={handleClose}
            >
                <Mui.MenuItem onClick={french}>
                    <Flags.FR title="France" style={{ width: '25px', height: '25px' }} />
                </Mui.MenuItem>
                <Mui.MenuItem onClick={english}>
                    <Flags.US title="United States" style={{ width: '25px', height: '25px' }} />
                </Mui.MenuItem>
                <Mui.MenuItem onClick={pirate}>
                    <Flags.IO title="Pirate" style={{ width: '25px', height: '25px' }} />
                </Mui.MenuItem>
            </Mui.Menu>
        </>
    );
}
