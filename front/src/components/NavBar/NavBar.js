import * as React from 'react';
import * as Mui from '@mui/material';
import NavBarLanguageButton from './NavBarLanguageButton';
import NavBarProfileButton from './NavBarProfileButton';
import NavBarAddButton from './NavBarAddButton';
import NavBarSearchField from './NavBarSearchField';
import NavBarEcoButton from './NavBarEcoButton';
import { useNavigate } from 'react-router-dom';
import { styled } from '@mui/material/styles';
import { useTranslation } from 'react-i18next';

export default function NavBar({ authState, isAdmin }) {
    const navigate = useNavigate();

    const Title = styled(Mui.Typography)(({ _ }) => ({
        '&:hover': {
            'cursor': 'pointer',
            'textDecoration': 'underline'
        }
    }));

    const [t] = useTranslation('common');

    return (
        <Mui.AppBar position="static">
            <Mui.Stack direction="row"
                justifyContent="space-evenly"
                alignItems="center">
                <img style={{ height: '50px' }} src='../img/favicon.png' onClick={() => navigate('/')} />
                <Title variant="h6" component="div" onClick={() => navigate('/')}>
                    {t('navbar.titre')}
                </Title>
                <Mui.Stack direction="row"
                    justifyContent="space-evenly"
                    alignItems="center">
                    <NavBarAddButton></NavBarAddButton>
                </Mui.Stack>
                <NavBarSearchField />
                <Mui.Stack direction="row"
                    justifyContent="center"
                    alignItems="center">
                    <Title variant="h6" component="div" onClick={() => navigate('/CarbonConsumption')}>
                        {t('navbar.consommationCarbone')}
                    </Title>
                    <NavBarEcoButton />
                    <NavBarLanguageButton></NavBarLanguageButton>
                    <NavBarProfileButton authState={authState} isAdmin={isAdmin}></NavBarProfileButton>
                </Mui.Stack>
            </Mui.Stack>
        </Mui.AppBar>
    );
}
