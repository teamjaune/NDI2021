import * as React from 'react';
import { styled, alpha } from '@mui/material/styles';
import InputBase from '@mui/material/InputBase';
import * as MuiIcons from '@mui/icons-material';
import { IconButton } from '@mui/material';
import Stack from '@mui/material/Stack';
import { useNavigate, useMatch } from 'react-router-dom';
import { useTranslation } from 'react-i18next';

export default function NavBarSearchField() {
    const match = useMatch('/Search/:searchText');
    const searchParams = match != null ? match.params.searchText : '';

    const navigate = useNavigate();

    const [searchText, setSearchText] = React.useState(searchParams);

    React.useEffect(() => {
        if (searchText !== undefined && searchText !== null && searchText !== '') {
            const searchInput = document.getElementById('searchInput');
            searchInput.value = searchText;
        }
    });

    const Search = styled('div')(({ theme }) => ({
        'position': 'relative',
        'borderRadius': theme.shape.borderRadius,
        'backgroundColor': alpha(theme.palette.common.white, 0.15),
        '&:hover': {
            'backgroundColor': alpha(theme.palette.common.white, 0.25)
        },
        'marginRight': theme.spacing(2),
        'marginLeft': 0,
        'width': '100%',
        [theme.breakpoints.up('sm')]: {
            'marginLeft': theme.spacing(3),
            'width': 'auto'
        }
    }));

    const StyledInputBase = styled(InputBase)(({ theme }) => ({
        'color': 'inherit',
        '& .MuiInputBase-input': {
            'padding': theme.spacing(1, 1, 1, 0),
            'paddingLeft': `calc(1em + ${theme.spacing(1)})`,
            'width': '100%'
        }
    }));

    const search = () => {
        if (searchText && searchText !== '') {
            navigate('/Search/' + searchText);
        }
    };

    const keyDown = (event) => {
        if (event.key === 'Enter') {
            search();
        }
    };

    const [t] = useTranslation('common');

    return (
        <>
            <Stack direction="row"
                justifyContent="space-evenly"
                sx={{ width: '50ch' }}
                alignItems="center">
                <Search>
                    <StyledInputBase
                        id="searchInput"
                        value={searchText}
                        autoComplete="off"
                        placeholder={t('recherche.placeholder')}
                        autoFocus={searchText !== null && searchText !== undefined}
                        inputProps={{ 'aria-label': 'search' }}
                        onChange={(event) => setSearchText(event.target.value)}
                        onKeyDown={(event) => keyDown(event)}
                    />
                    <IconButton onClick={search}>
                        <MuiIcons.Search htmlColor="#FFFFFF" />
                    </IconButton>
                </Search>
            </Stack>
        </>
    );
}
