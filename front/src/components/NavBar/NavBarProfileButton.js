import * as React from 'react';
import * as Mui from '@mui/material';
import * as MuiIcons from '@mui/icons-material';
import { getAuth } from 'firebase/auth';
import { useNavigate } from 'react-router-dom';
import { useTranslation } from 'react-i18next';

export default function NavBarProfileButton({ authState, isAdmin }) {
    const navigate = useNavigate();

    const [anchorEl, setAnchorEl] = React.useState(null);

    const handleMenu = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const dashBoard = () => {
        navigate('/AdminDashboard');
        handleClose();
    };

    const logOut = () => {
        getAuth().signOut();
    };

    const [t] = useTranslation('common');

    return (
        <>
            <Mui.IconButton
                size="large"
                aria-label="account of current user"
                aria-controls="menu-appbar"
                aria-haspopup="true"
                onClick={handleMenu}
                color="inherit"
            >
                <MuiIcons.AccountCircle />
            </Mui.IconButton>
            <Mui.Menu
                id="menu-appbar"
                anchorEl={anchorEl}
                anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'right'
                }}
                sx={{ my: 4 }}
                keepMounted
                transformOrigin={{
                    vertical: 'top',
                    horizontal: 'right'
                }}
                open={anchorEl !== null}
                onClose={handleClose}
            >
                {isAdmin && <Mui.MenuItem onClick={dashBoard}>
                    <MuiIcons.AdminPanelSettings />
                    {t('navbar.tableauDeBord')}
                </Mui.MenuItem>}
                {authState && <Mui.MenuItem onClick={logOut}>
                    <MuiIcons.Logout />
                    {t('navbar.deconnexion')}
                </Mui.MenuItem>}
                {!authState && <Mui.MenuItem onClick={() => navigate('/login')}>
                    <MuiIcons.Login />
                    {t('navbar.connexion')}
                </Mui.MenuItem>}
            </Mui.Menu>
        </>
    );
}
