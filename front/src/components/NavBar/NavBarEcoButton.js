import * as React from 'react';
import * as Mui from '@mui/material';
import * as MuiIcons from '@mui/icons-material';
import { useTranslation } from 'react-i18next';

export default function NavBarEcoButton() {
    const [open, setOpen] = React.useState(false);
    const [ecoFacts, setEcoFacts] = React.useState('');

    const [t] = useTranslation('common');

    const handleClickOpen = () => {
        setOpen(true);
        switch (Math.ceil(Math.random() * 11)) {
            case 1:
                setEcoFacts(t('fact.un'));
                break;
            case 2:
                setEcoFacts(t('fact.deux'));
                break;
            case 3:
                setEcoFacts(t('fact.trois'));
                break;
            case 4:
                setEcoFacts(t('fact.quatre'));
                break;
            case 5:
                setEcoFacts(t('fact.cinq'));
                break;
            case 6:
                setEcoFacts(t('fact.six'));
                break;
            case 7:
                setEcoFacts(t('fact.sept'));
                break;
            case 8:
                setEcoFacts(t('fact.huit'));
                break;
            case 9:
                setEcoFacts('Another one bites the dust');
                break;
            case 10:
                setEcoFacts('Show must go on');
                break;
            case 11:
                setEcoFacts('Mama, just killed a man');
                break;
            default:
                setEcoFacts('');
                break;
        }
    };

    const handleClose = () => {
        setOpen(false);
    };

    return (
        <div>
        <Mui.IconButton
            size='large'
            aria-label='account of current user'
            aria-controls='menu-appbar'
            aria-haspopup='true'
            onClick={handleClickOpen}
            color='inherit'
        >
            <MuiIcons.Park />
        </Mui.IconButton>
        <Mui.Dialog
            open={open}
            onClose={handleClose}
            aria-labelledby='alert-dialog-title'
            aria-describedby='alert-dialog-description'
        >
            <Mui.DialogTitle id='alert-dialog-title'>
            {t('fact.astuce')}
            </Mui.DialogTitle>
            <Mui.DialogContent>
            <Mui.DialogContentText id='alert-dialog-description'>
                {ecoFacts}
            </Mui.DialogContentText>
            </Mui.DialogContent>
            <Mui.DialogActions>
            <Mui.Button onClick={handleClose} autoFocus>{t('fact.fermer')}</Mui.Button>
            </Mui.DialogActions>
        </Mui.Dialog>
        </div>
    );
}
