import * as React from 'react';
import * as Mui from '@mui/material';
import * as MuiIcons from '@mui/icons-material';
import { useNavigate } from 'react-router-dom';
import { useTranslation } from 'react-i18next';

export default function NavBarAddButton() {
    const navigate = useNavigate();

    const [anchorEl, setAnchorEl] = React.useState(null);

    const handleMenu = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const addRescue = () => {
        handleClose();
        navigate('/AddRescue');
    };

    const addRescuer = () => {
        handleClose();
        navigate('/AddRescuer');
    };

    const addRescued = () => {
        handleClose();
        navigate('/AddRescued');
    };

    const [t] = useTranslation('common');

    return (
        <>
            <Mui.IconButton
                size="large"
                aria-label="account of current user"
                aria-controls="menu-appbar"
                aria-haspopup="true"
                onClick={handleMenu}
                color="inherit"
            >
                <Mui.Typography variant='h6'>{t('ajouter.add')}</Mui.Typography>
                <MuiIcons.Add />
            </Mui.IconButton>
            <Mui.Menu
                id="menu-appbar"
                anchorEl={anchorEl}
                anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'right'
                }}
                sx={{ my: 4 }}
                keepMounted
                transformOrigin={{
                    vertical: 'top',
                    horizontal: 'right'
                }}
                open={anchorEl !== null}
                onClose={handleClose}
            >
                <Mui.MenuItem onClick={addRescue}>
                    <MuiIcons.DirectionsBoat />
                    {t('navbar.ajouterSauvetage')}
                </Mui.MenuItem>
                <Mui.MenuItem onClick={addRescuer}>
                    <MuiIcons.Rowing />
                    {t('navbar.ajouterSauveteur')}
                </Mui.MenuItem>
                <Mui.MenuItem onClick={addRescued}>
                    <MuiIcons.Person />
                    {t('navbar.ajouterSauve')}
                </Mui.MenuItem>
            </Mui.Menu>
        </>
    );
}
