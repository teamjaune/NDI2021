/* eslint-disable no-undef */
export const ErrorType = {
    NO_ERROR: 'NO_ERROR',
    EMAIL: 'EMAIL',
    PASSWORD: 'PASSWORD',
    FIREBASE: 'FIREBASE',
    UNKNOWN: 'UNKNOWN',
    POPUP: 'POPUP'
}

/* eslint-enable no-undef */
export const ErrorMessages = {
    NO_ERROR: { message: '', type: ErrorType.NO_ERROR },
    INVALID_EMAIL: { message: 'Email is invalid.', type: ErrorType.EMAIL },
    SIGN_IN: { message: 'Incorrect email or password.', type: ErrorType.EMAIL },
    USER_EXISTS: { message: 'Email already in use !', type: ErrorType.EMAIL },
    PASSWORD_NOT_SECURE: { message: 'Password is not secure enough.', type: ErrorType.PASSWORD },
    PASSWORD_DO_NOT_MATCH: { message: 'Passwords do not match.', type: ErrorType.PASSWORD },
    FIREBASE_ERROR: { message: 'Please check your personal data.\nOtherwise try again later or contact the support.', type: ErrorType.FIREBASE },
    UNKNOWN_ERROR: { message: 'An error occured during registration.', type: ErrorType.UNKNOWN },
    POPUP_CLOSE: { message: 'Please do not close popups in order to use Google\'s services.', type: ErrorType.POPUP },
    POPUP_BLOCK: { message: 'Please allow popups in order to use Google\'s services.', type: ErrorType.POPUP }
}
