import * as React from 'react';
import * as Mui from '@mui/material';
import { getAuth, createUserWithEmailAndPassword } from 'firebase/auth';
import * as MuiIcons from '@mui/icons-material';
import { ErrorMessages, ErrorType } from './ErrorMessage';
import { useTranslation } from 'react-i18next';

export default function SignUp() {
  const [emailField, setEmailField] = React.useState('');
  const [passwordField, setPasswordField] = React.useState('');
  const [passwordFieldConfirm, setPasswordFieldConfirm] = React.useState('');
  const [errorMessage, setErrorMessage] = React.useState(ErrorMessages.NO_ERROR);
  const [passwordShown, setPasswordShown] = React.useState(false);
  const [repeatPasswordShown, setRepeatPasswordShown] = React.useState(false);

  const togglePasswordVisiblity = () => {
    setPasswordShown(!passwordShown);
  };
  const toggleRepeatPasswordVisiblity = () => {
    setRepeatPasswordShown(!repeatPasswordShown);
  };

  const handleSubmit = (event) => {
    event.preventDefault();
  };

  const signUpFirebaseEmail = () => {
    if (passwordField === passwordFieldConfirm) {
      const auth = getAuth();
      createUserWithEmailAndPassword(auth, emailField, passwordField)
        .catch(error => {
          switch (error.code) {
            case 'auth/invalid-email':
              setErrorMessage(ErrorMessages.INVALID_EMAIL);
              break;
            case 'auth/weak-password':
              setErrorMessage(ErrorMessages.PASSWORD_NOT_SECURE);
              break;
            case 'auth/email-already-in-use':
              setErrorMessage(ErrorMessages.USER_EXISTS);
              break;
            case 'auth/internal-error':
              setErrorMessage(ErrorMessages.FIREBASE_ERROR)
              break;
            default:
              setErrorMessage(ErrorMessages.UNKNOWN_ERROR);
              break;
          }
        })
    } else {
      setErrorMessage(ErrorMessages.PASSWORD_DO_NOT_MATCH);
    }
  };

  const keyDown = (event) => {
    if (event.key === 'Enter') {
      signUpFirebaseEmail();
    }
  };

  const [t] = useTranslation('common');

  return (
    <Mui.Box style={{ width: '60%' }}>
      {errorMessage.type !== ErrorType.NO_ERROR &&
        <Mui.Box sx={{ py: 1 }}>
          <Mui.Alert
            severity='error'
            action={
              <Mui.IconButton
                aria-label='close'
                color='inherit'
                size='small'
                onClick={() => {
                  setErrorMessage(ErrorMessages.NO_ERROR);
                }}
              >
                <MuiIcons.Close fontSize='inherit' />
              </Mui.IconButton>
            }
          >
            <strong>Error:</strong> {errorMessage.message}
          </Mui.Alert>
        </Mui.Box>
      }
      <Mui.Box component='form' noValidate onSubmit={handleSubmit} sx={{ mt: 1 }}>
        <Mui.Grid container spacing={2}>
          <Mui.Grid item xs={12}>
            <Mui.TextField
              error={errorMessage.type === ErrorType.EMAIL}
              margin='normal'
              required
              fullWidth
              id='email'
              label={t('login.email')}
              name='email'
              type='email'
              autoComplete='email'
              autoFocus
              value={emailField}
              onChange={(event) => setEmailField(event.target.value)}
              onKeyDown={(event) => keyDown(event)}
            />
          </Mui.Grid>
          <Mui.Grid item xs={12}>
            <Mui.TextField
              error={errorMessage.type === ErrorType.PASSWORD}
              margin='normal'
              required
              fullWidth
              name='password'
              label={t('login.motDePasse')}
              type={passwordShown ? 'text' : 'password'}
              id='password'
              autoComplete='new-password'
              value={passwordField}
              onChange={(event) => setPasswordField(event.target.value)}
              onKeyDown={(event) => keyDown(event)}
              InputProps={{
                endAdornment: (
                  <Mui.IconButton onClick={togglePasswordVisiblity} >
                    <Mui.Tooltip title={passwordShown ? t('login.cacher') : t('login.montrer')}>
                      {passwordShown ? <MuiIcons.Visibility color='primary' sx={{ mx: 1 }} /> : <MuiIcons.VisibilityOff color='primary' sx={{ mx: 1 }} />}
                    </Mui.Tooltip>
                  </Mui.IconButton >
                )
              }}
            />
          </Mui.Grid>
          <Mui.Grid item xs={12}>
            <Mui.TextField
              error={errorMessage.type === ErrorType.PASSWORD}
              margin='normal'
              required
              fullWidth
              name='passwordConfirm'
              label={t('login.confirmerMotDePasse')}
              type={repeatPasswordShown ? 'text' : 'password'}
              id='passwordConfirm'
              autoComplete='new-password'
              value={passwordFieldConfirm}
              onChange={(event) => setPasswordFieldConfirm(event.target.value)}
              onKeyDown={(event) => keyDown(event)}
              InputProps={{
                endAdornment: (
                  <Mui.IconButton onClick={toggleRepeatPasswordVisiblity} >
                    <Mui.Tooltip title={repeatPasswordShown ? t('login.cacher') : t('login.montrer')}>
                      {repeatPasswordShown ? <MuiIcons.Visibility color='primary' sx={{ mx: 1 }} /> : <MuiIcons.VisibilityOff color='primary' sx={{ mx: 1 }} />}
                    </Mui.Tooltip>
                  </Mui.IconButton >
                )
              }}
            />
          </Mui.Grid>
        </Mui.Grid>
        <Mui.Button
          type='submit'
          fullWidth
          variant='contained'
          sx={{ mt: 3, mb: 2 }}
          onClick={() => signUpFirebaseEmail()}
        >
          {t('login.inscrire')}
        </Mui.Button>
      </Mui.Box>
    </Mui.Box>
  );
}
