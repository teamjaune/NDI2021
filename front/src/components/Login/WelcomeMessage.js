import * as React from 'react'
import * as Mui from '@mui/material'
import { useTranslation } from 'react-i18next';

export default function WelcomeMessage(signIn = true) {
  const [t] = useTranslation('common');

  return (
    <Mui.Box
      sx={{
        width: '100%',
        height: '100%',
        backgroundColor: 'black',
        opacity: [0.8]
      }}
    >
      <Mui.Grid
        container
        alignContent='center'
        sx={{
          display: 'flex',
          flexDirection: 'column'
        }}
      >
        <Mui.Grid item
          sx={{
            p: 4,
            m: 4,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center'
          }}
        >
          <Mui.Typography variant='h2' color='white' textAlign='center'>
            {signIn ? t('login.retour') : t('login.bienvenue')}
          </Mui.Typography>
          <Mui.Typography
            variant='h4'
            color='white'
            letterSpacing='-1px'
            wordSpacing='-0.2px'
            fontWeight='700'
            fontStyle='bold'
            fontVariant='small-caps'
            textAlign='center'
            >
              {signIn ? t('login.revoir') : t('login.rejoindre')}
          </Mui.Typography>
        </Mui.Grid>
      </Mui.Grid>
    </Mui.Box>
  )
}
