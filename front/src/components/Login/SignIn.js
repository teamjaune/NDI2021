import * as React from 'react';
import * as Mui from '@mui/material';
import { getAuth, signInWithEmailAndPassword, setPersistence, browserSessionPersistence, indexedDBLocalPersistence } from 'firebase/auth';
import * as MuiIcons from '@mui/icons-material';
import { ErrorMessages, ErrorType } from './ErrorMessage';
import { useTranslation } from 'react-i18next';

export default function SignIn() {
  const [emailField, setEmailField] = React.useState('');
  const [passwordField, setPasswordField] = React.useState('');
  const [errorMessage, setErrorMessage] = React.useState(ErrorMessages.NO_ERROR);
  const [passwordShown, setPasswordShown] = React.useState(false); // Use to show or not the password
  const [rememberMe, setRememberMe] = React.useState(false);
  const auth = getAuth();

  const togglePasswordVisiblity = () => {
    setPasswordShown(!passwordShown);
  };

  const handleSubmit = (event) => {
    event.preventDefault();
  };

  const handleChangeRememberMe = () => {
    setRememberMe(!rememberMe);
    setPersistence(auth, rememberMe ? browserSessionPersistence : indexedDBLocalPersistence)
    .catch(error => {
      if (error.code != null) {
        setErrorMessage(ErrorMessages.UNKNOWN_ERROR);
      }
    });
  };

  const signInFirebaseEmail = () => {
    signInWithEmailAndPassword(auth, emailField, passwordField)
    .catch(error => {
      if (error.code != null) {
        setErrorMessage(ErrorMessages.SIGN_IN);
      }
    });
  };

  const keyDown = (event) => {
    if (event.key === 'Enter') {
      signInFirebaseEmail();
    }
  };

  const [t] = useTranslation('common');

  return (
    <Mui.Box style={{ width: '60%' }}>
      {errorMessage.type !== ErrorType.NO_ERROR &&
        <Mui.Box sx={{ py: 1 }}>
          <Mui.Alert
            severity='error'
            action={
              <Mui.IconButton
                aria-label='close'
                color='inherit'
                size='small'
                onClick={() => {
                  setErrorMessage(ErrorMessages.NO_ERROR);
                }}
              >
                <MuiIcons.Close fontSize='inherit' />
              </Mui.IconButton>
            }
          >
            <strong>Error:</strong> {errorMessage.message}
          </Mui.Alert>
        </Mui.Box>
      }
      <Mui.Box component='form' noValidate onSubmit={handleSubmit} sx={{ mt: 1 }}>
        <Mui.Grid container spacing={2}>
          <Mui.Grid item xs={12}>
            <Mui.TextField
              error={errorMessage.type !== ErrorType.NO_ERROR && emailField.length === 0}
              margin='normal'
              required
              fullWidth
              id='email'
              label={t('login.email')}
              name='email'
              type='email'
              autoComplete='current-email'
              autoFocus
              value={emailField}
              onChange={(event) => setEmailField(event.target.value)}
              onKeyDown={(event) => keyDown(event)}
            />
          </Mui.Grid>
          <Mui.Grid item xs={12}>
            <Mui.TextField
              error={errorMessage.type !== ErrorType.NO_ERROR && passwordField.length === 0}
              margin='normal'
              required
              fullWidth
              name='password'
              label={t('login.motDePasse')}
              type={passwordShown ? 'text' : 'password'}
              id='password'
              autoComplete='current-password'
              value={passwordField}
              onChange={(event) => setPasswordField(event.target.value)}
              onKeyDown={(event) => keyDown(event)}
              InputProps={{
                endAdornment: (
                  <Mui.IconButton onClick={togglePasswordVisiblity} >
                    <Mui.Tooltip title={passwordShown ? t('login.cacher') : t('login.montrer')}>
                      {passwordShown
                        ? <MuiIcons.Visibility color='primary' sx={{ mx: 1 }} />
                        : <MuiIcons.VisibilityOff color='primary' sx={{ mx: 1 }} />}
                    </Mui.Tooltip>
                  </Mui.IconButton >
                )
              }}
            />
          </Mui.Grid>
          <Mui.Grid item xs={12}>
            <Mui.FormControlLabel
              control={<Mui.Checkbox value={rememberMe} onChange={handleChangeRememberMe} color='primary' />}
              label={t('login.seSouvenirDeMoi')}
            />
          </Mui.Grid>
        </Mui.Grid>
        <Mui.Button
          type='submit'
          fullWidth
          variant='contained'
          sx={{ mt: 3, mb: 2 }}
          onClick={() => signInFirebaseEmail()}
        >
          {t('login.authentifier')}
        </Mui.Button>
        <Mui.Grid>
          <Mui.Link href='#' variant='body2'>
            {t('login.forgotPassword')}
          </Mui.Link>
        </Mui.Grid>
      </Mui.Box>
    </Mui.Box>
  );
}
