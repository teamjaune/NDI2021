import React from 'react';
import LoginPage from './pages/LoginPage';
import MainPage from './pages/MainPage';
import AddRescuePage from './pages/Add/AddRescuePage';
import AddRescuerPage from './pages/Add/AddRescuerPage';
import AddRescuedPage from './pages/Add/AddRescuedPage';
import AdminDashboard from './pages/AdminDashboard/AdminDashboardPage';
import RescuerPage from './pages/RescuerPage';
import RescuedPage from './pages/RescuedPage';
import RescuePage from './pages/RescuePage';
import SearchPage from './pages/SearchPage';
import CarbonConsumptionPage from './pages/CarbonConsumptionPage';
import NavBar from './components/NavBar/NavBar';
import { Routes, Route, Navigate } from 'react-router-dom';
import { getAuth, onAuthStateChanged } from 'firebase/auth';
import { CircularProgress, Stack } from '@mui/material';
import Communication from './service/communication';

export default function App() {
  const [isAuthed, setAuthed] = React.useState(undefined);
  const [isAdmin, setAdmin] = React.useState(false);

  React.useEffect(() => {
    onAuthStateChanged(getAuth(), async (user) => {
      if (user) {
        if (!isAuthed) {
          user.getIdTokenResult().then((idTokenResult) => {
            if (idTokenResult.claims && idTokenResult.claims.admin) {
              setAdmin(true);
            }
          })
          await Communication.sendGetRequest('api/public/login/', await user.getIdToken())
            .then(async (response) => {
              if (response.status === 200) {
                setAuthed(true);
              }
            });
        }
      } else {
        setAuthed(false);
      }
    });
  }, []);

  if (isAuthed === undefined) {
    return <AppLoading />;
  }

  return (
    <Routes>
      <Route path='/login' element={!isAuthed ? <LoginPage /> : <Navigate to='/' />} />
      <Route path='/' element={<div style={{ marginBottom: 20 }}>
        <NavBar authState={isAuthed} isAdmin={isAdmin} /><MainPage /></div>} />
      <Route path='/Rescuer/:id' element={<div style={{ marginBottom: 20 }}>
        <NavBar authState={isAuthed} isAdmin={isAdmin} /><RescuerPage /></div>} />
      <Route path='/Rescued/:id' element={<div style={{ marginBottom: 20 }}>
        <NavBar authState={isAuthed} isAdmin={isAdmin} /><RescuedPage /></div>} />
      <Route path='/Rescue/:id' element={<div style={{ marginBottom: 20 }}>
        <NavBar authState={isAuthed} isAdmin={isAdmin} /><RescuePage /></div>} />
      <Route path='/Search/:searchText' element={<div style={{ marginBottom: 20 }}>
        <NavBar authState={isAuthed} isAdmin={isAdmin} /><SearchPage /></div>} />
      <Route path='/*' element={
        <RequireAuth redirectTo="/login" authState={isAuthed}>
          <MainAppRouter authState={isAuthed} isAdmin={isAdmin} />
        </RequireAuth>
      } />
    </Routes>
  );
}

function RequireAuth({ children, redirectTo, authState }) {
  return authState ? children : <Navigate to={redirectTo} />;
}

function MainAppRouter({ isAdmin, authState }) {
  return (
    <div style={{ marginBottom: 20 }}>
      <NavBar authState={authState} isAdmin={isAdmin} />
      <Routes>
        <Route path='/CarbonConsumption' element={<CarbonConsumptionPage />} />
        <Route path='/AddRescue' element={<AddRescuePage />} />
        <Route path='/AddRescuer' element={<AddRescuerPage />} />
        <Route path='/AddRescued' element={<AddRescuedPage />} />
        <Route path='/AdminDashboard' element={
        <RequireAuth redirectTo="/login" authState={isAdmin}>
          <AdminDashboard />
        </RequireAuth>
      } />
      </Routes>
    </div>
  );
}

function AppLoading() {
  return (
    <Stack alignItems='center'>
      <CircularProgress></CircularProgress>
    </Stack>
  );
}
