# bin/bash
cd ./docker-compose-lamp
docker-compose down
cd ..
cd ./front
 git pull
 npm i
 rm -rf build
 npm run build
cd ..
rm -rf ./back/src/main/resources/static
mv ./front/build/ ./back/src/main/resources/static/
cd ./docker-compose-lamp
docker-compose up -d
